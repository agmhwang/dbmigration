#!/bin/bash
main () {
 if [ $1 = 'dump' ]; then
   echodb $2
   lolist=$(psql -h $2 -U hwang -A -t -c "\lo_list" portal_db | tail -n +3 | head -n -1)
   for lo in `echo $lolist`; do
     id=$(echo $lo | awk -F '|' '{ print $1 }')
     owner=$(echo $lo | awk -F '|' '{ print $2 }')
     if [ ! -e loexport ]; then
       mkdir loexport
     fi
     psql -h $2 -U hwang -c "\lo_export $id ./loexport/$owner|$id" portal_db
   done
 elif [ $1 = 'load' ]; then
   echodb $2
   echo "confirm? y/n"
   read cf
   if [ $cf != "y" ]; then
     echo "exit."
     exit 1
   fi
   if [ ! -e loexport ]; then
     echo "loexport dir does not exist."
     exit 1
   fi
   for name in `ls -1 loexport`; do
     id=$(echo $name | awk -F '|' '{ print $2 }')
     owner=$(echo $name | awk -F '|' '{ print $1 }')
     psql -h $2 -U $owner -c "\lo_import ./loexport/$name $id" portal_db
    done
 else 
   echo "need either dump <db addr> or load <db addr>."
 fi
}

echodb () {
  echo 'DB address is: '$1
}
main $1 $2
