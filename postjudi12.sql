--JUDI-2589
ALTER TABLE usertrialprotocoltraining ADD COLUMN created timestamp;
UPDATE usertrialprotocoltraining set created = now() WHERE created is null;
ALTER TABLE usertrialprotocoltraining ALTER COLUMN created SET DEFAULT now();
ALTER TABLE usertrialprotocoltraining ALTER COLUMN created SET NOT NULL;
ALTER TABLE usertrialprotocoltraining ADD COLUMN updated timestamp;
UPDATE usertrialprotocoltraining set updated = created;
ALTER TABLE usertrialprotocoltraining ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE usertrialprotocoltraining ALTER COLUMN updated SET NOT NULL;
GRANT ALL ON TABLE usertrialprotocoltraining TO "spring";

-- change NULLs to non-nulls, as needed to keep 3.14.x model happy.
-- These changes are needed for Imaging tables being used by Judi (Judi-imaging) for the first time
alter table portal_db.public.study 
	alter column timesupdated set default 0
	,alter column transfertimedout set default false
	;
update portal_db.public.study set timesupdated=0 where timesupdated is null;
update portal_db.public.study set transfertimedout=false where transfertimedout is null;

ALTER TABLE trialsetting OWNER TO agmednet;

--JUDI-3506
alter table usertrialprotocoltraining drop constraint IF EXISTS usertrialprotocoltraining_trial_id_useraccount_id_key;


--JUDI-3549
CREATE TABLE bonita.eventtypetrialmetadataxref (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    trialmetadata_id BIGINT NOT NULL REFERENCES bonita.trialmetadata(id),
    eventtype_id BIGINT NOT NULL REFERENCES bonita.eventtype(id)
);
--Do we need to create a unique index?
CREATE UNIQUE INDEX "trialmetadata_eventtype_key" ON bonita.eventtypetrialmetadataxref(trialmetadata_id,eventtype_id);
GRANT ALL ON TABLE bonita.eventtypetrialmetadataxref TO "spring";


ALTER TABLE bonita.eventtypetrialmetadataxref ADD COLUMN required boolean NOT NULL default false ;


-- added 21-SEP-2016 WEB-4752
ALTER TABLE trial ALTER COLUMN telerad DROP NOT NULL;

--JUDI-3615
CREATE TABLE bonita.datadictionary (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    trial_id BIGINT NOT NULL REFERENCES trial(id),
    codevalue text NOT NULL,
	shortval text,
	longval text,
	otherval text,
	CHECK ( shortval IS NOT NULL or longval IS NOT NULL or otherval IS NOT NULL )
);
CREATE UNIQUE INDEX "trial_codevalue_key" ON bonita.datadictionary(trial_id,codevalue);
GRANT ALL ON TABLE bonita.datadictionary TO "spring";

CREATE TABLE bonita.datadictionary_grouping (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    trial_id BIGINT NOT NULL REFERENCES trial(id),
    datadictionary_id BIGINT NOT NULL REFERENCES bonita.datadictionary(id),
	format text NOT NULL,
	name text NOT NULL
);
GRANT ALL ON TABLE bonita.datadictionary_grouping TO "spring";
CREATE UNIQUE INDEX "trial_name_key" ON bonita.datadictionary_grouping(trial_id,name);


CREATE TABLE bonita.datadictionary_grouping_option (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    datadictionary_grouping_id BIGINT NOT NULL REFERENCES bonita.datadictionary_grouping(id),
    datadictionary_id BIGINT NOT NULL REFERENCES bonita.datadictionary(id)
);
GRANT ALL ON TABLE bonita.datadictionary_grouping_option TO "spring";

--JUDI-3834
CREATE OR REPLACE FUNCTION updateJudiEmails() RETURNS text AS $$
	DECLARE
		trial RECORD;
		template RECORD;
		result text;
	BEGIN
		FOR trial IN SELECT * from trial where productbitmask = 2 LOOP
			FOR template in select * from portalemailtemplate pet where pet.templatename like '%' || trial.name ||'%' and pet.autotextfooter = 'footerForTrials' LOOP
				update portalemailtemplate set autotextfooter = 'footerForJudi' where id = template.id;
			END LOOP;
		END LOOP;
		return result;
	END;
$$ LANGUAGE plpgsql;

SELECT * from updateJudiEmails();
drop function updateJudiEmails();
GRANT ALL ON TABLE bonita.datadictionary_grouping_option TO "spring";


UPDATE study SET countOfVisibleStudyForms = 0 WHERE countOfVisibleStudyForms is null;
ALTER TABLE study ALTER COLUMN countOfVisibleStudyForms SET NOT NULL;
ALTER TABLE study ALTER COLUMN countOfVisibleStudyForms SET DEFAULT 0;


UPDATE study SET expectedInstanceCount = 0 WHERE expectedInstanceCount is null;
ALTER TABLE study ALTER COLUMN expectedInstanceCount SET NOT NULL;
ALTER TABLE study ALTER COLUMN expectedInstanceCount SET DEFAULT 0;



UPDATE study SET expectedSeriesCount = 0 WHERE expectedSeriesCount is null;
ALTER TABLE study ALTER COLUMN expectedSeriesCount SET NOT NULL;
ALTER TABLE study ALTER COLUMN expectedSeriesCount SET DEFAULT 0;

UPDATE study SET instanceCountLocked = false WHERE instanceCountLocked is null;
ALTER TABLE study ALTER COLUMN instanceCountLocked SET NOT NULL;
ALTER TABLE study ALTER COLUMN instanceCountLocked SET DEFAULT false;

-- Adding a fixed length delimiter column. I'm making the assumption that delimiters won't be long strings of text.
ALTER TABLE trial ADD COLUMN delimiter char;


-- 29-SEP-2016 - fix userrole constraints so that a global role cannot insert if there is a custom role with the same name & vice versa
ALTER TABLE userrole DROP CONSTRAINT "RoleIsUniqueWithinTrial";

CREATE UNIQUE INDEX "RoleIsUniqueWithinTrialWithTrialId" ON userrole (trial_id, name)WHERE trial_id IS NOT NULL;
CREATE UNIQUE INDEX "RoleIsUniqueWithinTrialNoTrialId" ON userrole (name)WHERE trial_id IS NULL;
    
CREATE OR REPLACE FUNCTION check_no_role_name_conflict()
  RETURNS trigger AS
$BODY$
begin 
    if NEW.trial_id IS NOT NULL then
		--make sure this is not a global name
		if exists (select * from userrole WHERE trial_id IS NULL AND (name = NEW.name OR displayname = NEW.name)) then
			raise EXCEPTION 'userrole name already exists as a global role';			
		end if;
	else
		--make sure this is not a custom name
		if exists (select * from userrole WHERE trial_id IS NOT NULL AND name = NEW.name) then
			raise EXCEPTION 'userrole name already exists as a custom role';			
		end if;
	end if;
    return new;
end
$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER custom_role_may_not_have_global_name
    BEFORE INSERT OR UPDATE ON userrole
    FOR EACH ROW
    EXECUTE PROCEDURE check_no_role_name_conflict();
	
-- fix audit log entries
update historyevent set xml = replace(xml, 'adjudicationAuditHistoryRecord', 'auditHistoryRecord') WHERE service = 'JUDI' AND XML ilike '%adjudicationAuditHistoryRecord%';
--JUDI-3818
CREATE TABLE bonita.eventtypequery 
(
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    eventtype_id BIGINT NOT NULL REFERENCES bonita.eventtype(id),
    query text NOT NULL,
	type text NOT NULL --should this be text or should there be an enum?
);
GRANT ALL ON TABLE bonita.eventtypequery TO "spring";
--Possibly?
CREATE UNIQUE INDEX "uniqueQueryTypePerEventType" ON bonita.eventtypequery (eventtype_id, type);

ALTER TABLE bonita.eventinstance ADD COLUMN onhold BOOLEAN DEFAULT false;
UPDATE bonita.eventinstance SET onhold = false WHERE onhold IS NULL;
ALTER TABLE bonita.eventinstance ALTER COLUMN onhold SET NOT NULL;

alter table bonita.eventtype add column trialmetadataforform bigint references bonita.trialmetadata(id);

INSERT INTO bonita.user_ (tenantid,id,username,password,firstname,lastname,manageruserid,createdby,creationdate,lastupdate,enabled) VALUES(1,nextval('public.hibernate_sequence'),'judiadmin','WN1M3tyEGjj7E1RCGBX1EOIvFpw=','Judge','judi',0,-1,1429897677997,1429897677997,true);

INSERT INTO bonita.user_contactinfo (tenantid,id,userid,personal) VALUES(1,nextval('public.hibernate_sequence'),(select id from bonita.user_ WHERE username = 'judiadmin'),true);

INSERT INTO bonita.user_membership(tenantid, id ,userid ,roleid ,groupid, assignedby, assigneddate) VALUES (1,nextval('public.hibernate_sequence'),(select id from bonita.user_ WHERE username = 'judiadmin'),(select id from bonita.role where name = 'Administrator'),1,-1,1438020307334);

-- JUDI-3898: Adding XML Type for configuration of editing events.
INSERT INTO bonita.xmltype(id,version,name,created,updated) VALUES (3,null,'metadatas',now(),now());

CREATE TABLE bonita.linkedeventinstance 
(
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    parenteventinstance_id BIGINT NOT NULL REFERENCES bonita.eventinstance(id),
    childeventinstance_id BIGINT NOT NULL REFERENCES bonita.eventinstance(id),
	linktype text NOT NULL
);
GRANT ALL ON TABLE bonita.linkedeventinstance TO "spring";
ALTER TABLE bonita.trialmetadata ADD COLUMN displayname text DEFAULT null;

ALTER TABLE public.trial ADD COLUMN originalmetadata boolean DEFAULT false;

UPDATE public.trial SET originalmetadata = true;

ALTER TABLE report DROP CONSTRAINT IF EXISTS  reportname_key;

INSERT INTO public.portalemailtemplate (id, version,agmemailtemplate,bodyhtmltemplate,bodymessagetemplate,bodytexttemplate,fromemailaddress,fromname,subject,templatename,userrolebitmask) values((SELECT id FROM public.portalemailtemplate ORDER BY id DESC LIMIT 1) +1 ,current_timestamp,TRUE,'','','TBD','do_not_reply@agmednet.com','AG Mednet, Inc.','CSV Summary Email','summary_email',0);
ALTER TABLE bonita.eventtypestate ADD COLUMN json TEXT;

-- The expected format for a date input (for CSV/SFTP Event Sync), i.e. MM-dd-yyyy
ALTER TABLE bonita.trialmetadata ADD COLUMN dateformat TEXT;

CREATE TABLE sftp_server
(
 id bigint NOT NULL DEFAULT nextval('hibernate_sequence'::regclass),
  host character varying,
  port bigint DEFAULT 22,
  username character varying,
  password character varying,
  allow_unknown_keys boolean NOT NULL DEFAULT true,
  local_directory character varying,
  remote_directory character varying,
  delete_remote_files boolean NOT NULL DEFAULT true,
  local_directory_done character varying,
  local_directory_error character varying,
  trial_id bigint,
  headers boolean NOT NULL DEFAULT true,
  version timestamp without time zone,
  CONSTRAINT pk_sftp_id PRIMARY KEY (id),
  CONSTRAINT fk_sftp_trial_idx FOREIGN KEY (trial_id)
      REFERENCES trial (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sftp_server
  OWNER TO agmednet;

-- Index: fki_sftp_trial_idx

-- DROP INDEX fki_sftp_trial_idx;

CREATE INDEX fki_sftp_trial_idx
  ON sftp_server
  USING btree
  (trial_id);

ALTER TABLE sftp_server ADD COLUMN editevents BOOLEAN NOT NULL DEFAULT FALSE;

-- daubin
ALTER TABLE study ALTER COLUMN deleted SET NOT NULL;

--tperkins

INSERT INTO reportformat (id,name) VALUES(5,'txt');
INSERT INTO reportformat (id,name) VALUES(6,'csv');
--For Judi-imaging
-- ALTER TABLE bonita.documenttype DROP COLUMN imaging;
ALTER TABLE bonita.documenttype ADD COLUMN imaging boolean;
ALTER TABLE bonita.documenttype ALTER COLUMN imaging SET DEFAULT false;

ALTER TABLE bonita.baselinedossier
  OWNER TO bonita;
GRANT ALL ON TABLE bonita.baselinedossier TO bonita;
GRANT SELECT ON TABLE bonita.baselinedossier TO readonly;
GRANT ALL ON TABLE bonita.baselinedossier TO agmednet;
GRANT ALL ON TABLE bonita.baselinedossier TO public;

-- JUDI-4730
CREATE UNIQUE INDEX sftp_server_unique_host_info ON sftp_server (trial_id,host,port,remote_directory);
UPDATE bonita.formresult SET xml = replace(xml,'&#x27;','&#39;') WHERE xml ilike '%&#x27;%';
UPDATE bonita.formresult SET xml = replace(xml,'%20',' ') WHERE xml ~ '%20';
UPDATE bonita.ecrfresult SET xml = replace(xml,'&#x27;','&#39;') WHERE xml ilike '%&#x27;%';
UPDATE bonita.ecrfresult SET xml = replace(xml,'%20',' ') WHERE xml ~ '%20';
UPDATE bonita.reviewerformresult SET xml = replace(xml,'&#x27;','&#39;') WHERE xml ilike '%&#x27;%';
UPDATE bonita.reviewerformresult SET xml = replace(xml,'%20',' ') WHERE xml ~ '%20';
UPDATE bonita.finaloutcome SET xml = replace(xml,'&#x27;','&#39;') WHERE xml ilike '%&#x27;%';
UPDATE bonita.finaloutcome SET xml = replace(xml,'%20',' ') WHERE xml ~ '%20';CREATE TABLE sftpserverreport
(
  id bigint NOT NULL DEFAULT nextval('hibernate_sequence'::regclass),
  host character varying,
  port bigint,
  username character varying,
  password character varying,
  location character varying,
  trial_id bigint NOT NULL,
  schedule character varying,
  version timestamp without time zone,
  CONSTRAINT pk_sftpserverreport_id PRIMARY KEY (id),
  CONSTRAINT fk_sftpserverreport_trial_idx FOREIGN KEY (trial_id)
      REFERENCES trial (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sftpserverreport OWNER TO agmednet;
-- JUDI-4835
DO $$
  BEGIN
    ALTER TABLE bonita.document ADD COLUMN study_id BIGINT REFERENCES public.study(id);
  EXCEPTION
    WHEN duplicate_column THEN RAISE NOTICE 'column study_id already exists in bonita.document. %', SQLERRM;
END $$;
-- I would like to add a not null, but since our code is in a few different projects, i'd be afraid that some code somewhere might be trying to specifically enter a null value and break
DO $$
  BEGIN
    ALTER TABLE public.study ADD COLUMN productbitmask BIGINT DEFAULT 1;
  EXCEPTION
    WHEN duplicate_column THEN RAISE NOTICE 'column productbitmask already exists in public.study. %', SQLERRM;
END $$;
-- the study will always point to the document so that there is a history of all studies uploaded for a document. So when a study is uploaded as a replacement, we won't lose the link
-- in short bonita.document.study_id shows the "active" study for a given document. While public.study.document_id will show the entire list of studies for a given document
DO $$
  BEGIN
    ALTER TABLE public.study ADD COLUMN document_id BIGINT REFERENCES bonita.document(id);
  EXCEPTION
    WHEN duplicate_column THEN RAISE NOTICE 'column document_id already exists in public.study. %', SQLERRM;
END $$;
-- a study should only be active for one document
DO $$
  BEGIN
    CREATE UNIQUE INDEX document_study_unique ON bonita.document (study_id);
  EXCEPTION
    WHEN duplicate_table THEN RAISE NOTICE 'index document_study_unique not created on bonita.document (study_id) % %', SQLERRM, SQLSTATE;
END $$;

-- JUDI-4873 Review study columns null constraint for imaging judi merge
ALTER TABLE public.study ALTER COLUMN countofvisiblestudyforms DROP NOT NULL;
ALTER TABLE public.study ALTER COLUMN instancecountlocked DROP NOT NULL;
ALTER TABLE public.study ALTER COLUMN expectedinstancecount DROP NOT NULL;
ALTER TABLE public.study ALTER COLUMN expectedseriescount DROP NOT NULL;

-- JUDI-4762 Review trial columns null constraint for imaging judi merge
ALTER TABLE public.trial ALTER COLUMN requireadduserconfirmation DROP NOT NULL;
ALTER TABLE public.trial ALTER COLUMN blockformedits DROP NOT NULL;
ALTER TABLE public.trial ALTER COLUMN cloudenabled DROP NOT NULL;
ALTER TABLE public.trial ALTER COLUMN informonly DROP NOT NULL;
ALTER TABLE public.trial ALTER COLUMN hasredactabledocuments DROP NOT NULL;

-- JUDI-4763 Review trialsite columns null constraint for imaging judi merge
ALTER TABLE public.trialsite ALTER COLUMN telerad DROP NOT NULL;
ALTER TABLE public.trialsite ALTER COLUMN defaultfortrial DROP NOT NULL;
ALTER TABLE public.trialsite ALTER COLUMN ctmdeleted DROP NOT NULL;

-- JUDI-4764 Review trialsubject columns null constraint for imaging judi merge
ALTER TABLE public.trialsubject ALTER COLUMN ctmdeleted DROP NOT NULL;
ALTER TABLE public.trialsubject ALTER COLUMN enabled DROP NOT NULL;

-- JUDI-4765 Review useraccount columns null constraint for imaging judi merge
ALTER TABLE public.useraccount ALTER COLUMN beentotraining DROP NOT NULL;
ALTER TABLE public.useraccount ALTER COLUMN loggedintodaatleastonce DROP NOT NULL;
ALTER TABLE public.useraccount ALTER COLUMN visitedexamlist DROP NOT NULL;

-- JUDI-4898 - tables for granting roles to roles	
-- drop table if exists rolegrantedtorole;
CREATE TABLE public.rolegrantedtorole 
(
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL default now(),
    updated timestamp without time zone NOT NULL default now(),
    trial_id BIGINT NOT NULL REFERENCES trial(id),    
    targetrole_id BIGINT NOT NULL REFERENCES userrole(id),
    grantedrole_id BIGINT NOT NULL REFERENCES userrole(id)
);

grant all privileges on table public.rolegrantedtorole to "servicemix-securitymanager";
grant all privileges on table public.rolegrantedtorole to "agmednet";

-- JUDI-4900: tracks hidden roles given to users, analagous to usersroleontrial
CREATE TABLE
    public.hiddenroleontrial
    (
        id BIGINT NOT NULL,
        version TIMESTAMP(6) WITHOUT TIME ZONE,
        trial_id BIGINT,
        trialsite_id BIGINT,
        useraccount_id BIGINT NOT NULL,
        userrole_id BIGINT NOT NULL,
        PRIMARY KEY (id),
        CONSTRAINT hiddenroleontrial_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES trial (id),
        CONSTRAINT hiddenroleontrial_trialsite_id_fkey FOREIGN KEY (trialsite_id) REFERENCES
        trialsite (id),
        CONSTRAINT hiddenroleontrial_useraccount_id_fkey FOREIGN KEY (useraccount_id) REFERENCES
        useraccount (id),
        CONSTRAINT hiddenroleontrial_userrole_id_fkey FOREIGN KEY (userrole_id) REFERENCES userrole
        (id)
    ); 
    
-- JUDI-4901: added trialManager role
insert into userrole (id, version, conditional, name, productBitmask, roleBitmask, actionBitmask, sitebased,trial_id, displayName, accessreporting) values (nextval('public.hibernate_sequence'), current_timestamp, false, 'trialSender',0,0,4096,false,null,'Trial Sender',false);
-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element_type (
    dicom_element_type_id INT PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);
ALTER TABLE bonita.dicom_element_type OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element_public_type (
    dicom_element_public_type_id INT PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);
ALTER TABLE bonita.dicom_element_public_type OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element_value_representation (
    dicom_element_value_representation_id INT PRIMARY KEY,
	name_short TEXT NOT NULL UNIQUE,
	name_long TEXT NULL,
	definition TEXT NULL,
	character_repertoire TEXT NULL,
	length_of_value TEXT NULL
);
ALTER TABLE bonita.dicom_element_value_representation OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element (
    dicom_element_id BIGSERIAL PRIMARY KEY,
    dicom_element_type_id INT NOT NULL
        REFERENCES bonita.dicom_element_type(dicom_element_type_id),
    dicom_element_value_representation_id INT NULL 
        REFERENCES bonita.dicom_element_value_representation(dicom_element_value_representation_id)
);
ALTER TABLE bonita.dicom_element OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element_public (
    dicom_element_id BIGINT PRIMARY KEY 
    	REFERENCES bonita.dicom_element(dicom_element_id),
    dicom_element_public_type_id INT NOT NULL
    	REFERENCES bonita.dicom_element_public_type(dicom_element_public_type_id),
    group_number TEXT NOT NULL,
    element_number TEXT NOT NULL,
    name TEXT NULL,
    keyword TEXT NULL,
    value_multiplicity_range TEXT NULL,
    UNIQUE(group_number, element_number)
);
ALTER TABLE bonita.dicom_element_public OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element_private_subgroup (
    dicom_element_private_subgroup_id SERIAL PRIMARY KEY,
    subgroup_identification_code TEXT NOT NULL,
    group_number TEXT NOT NULL,
    UNIQUE(group_number, subgroup_identification_code)
);
ALTER TABLE bonita.dicom_element_private_subgroup OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.dicom_element_private (
    dicom_element_id BIGINT PRIMARY KEY
    	REFERENCES bonita.dicom_element(dicom_element_id),
    dicom_element_private_subgroup_id INT NOT NULL 
        REFERENCES bonita.dicom_element_private_subgroup(dicom_element_private_subgroup_id),
    subgroup_element_number TEXT NOT NULL,
    value_multiplicity_range TEXT NULL,
    UNIQUE(dicom_element_private_subgroup_id, subgroup_element_number)
);
ALTER TABLE bonita.dicom_element_private OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------

/******************************************************************************/
/* BEGIN INSERT DATA                                                          */


/*----------------------------------------------------------------------------*/
/* BEGIN INSERT INTO TABLE bonita.dicom_element_type                          */


INSERT INTO bonita.dicom_element_type (dicom_element_type_id, name)
    VALUES (1, 'public');
INSERT INTO bonita.dicom_element_type (dicom_element_type_id, name)
    VALUES (2, 'private');
 
 
/* END INSERT INTO TABLE bonita.dicom_element_type                            */
/*----------------------------------------------------------------------------*/
/* BEGIN INSERT INTO TABLE bonita.dicom_element_public_type                   */


INSERT INTO bonita.dicom_element_public_type (dicom_element_public_type_id, name)
    VALUES (1, 'data');
INSERT INTO bonita.dicom_element_public_type (dicom_element_public_type_id, name)
    VALUES (2, 'file_meta');
INSERT INTO bonita.dicom_element_public_type (dicom_element_public_type_id, name)
    VALUES (3, 'directory_structuring');
 
 
/* END INSERT INTO TABLE bonita.dicom_element_public_type                     */
/*----------------------------------------------------------------------------*/


/* END INSERT DATA                                                            */
/******************************************************************************/
  /******************************************************************************/
/* BEGIN INSERT DATA                                                          */


/*----------------------------------------------------------------------------*/
/* BEGIN INSERT INTO TABLE bonita.dicom_element_value_representation          */
   
   
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (1,'AE','Application Entity','Default Character Repertoire excluding character code 5CH (the BACKSLASH "\" in ISO-IR 6), and control characters LF, FF, CR and ESC.','16 bytes maximum','A string of characters that identifies an Application Entity with leading and trailing spaces (20H) being non-significant. A value consisting solely of spaces shall not be used.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (2,'AS','Age String','""0""-""9"", ""D"", ""W"", ""M"", ""Y"" of Default Character Repertoire','4 bytes fixed','A string of characters with one of the following formats -- nnnD, nnnW, nnnM, nnnY; where nnn shall contain the number of days for D, weeks for W, months for M, or years for Y.
Example: ""018M"" would represent an age of 18 months.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (3,'AT','Attribute Tag','not applicable','4 bytes fixed','Ordered pair of 16-bit unsigned integers that is the value of a Data Element Tag.
Example: A Data Element Tag of (0018,00FF) would be encoded as a series of 4 bytes in a Little-Endian Transfer Syntax as 18H,00H,FFH,00H and in a Big-Endian Transfer Syntax as 00H,18H,00H,FFH.
Note

The encoding of an AT value is exactly the same as the encoding of a Data Element Tag as defined in Section 7.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (4,'CS','Code String','Uppercase characters, "0"-"9", the SPACE character, and underscore "_", of the Default Character Repertoire','16 bytes maximum','A string of characters with leading or trailing spaces (20H) being non-significant.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (5,'DA','Date','""0""-""9"" of Default Character Repertoire
In the context of a Query with range matching (see PS3.4), the character ""-"" is allowed, and a trailing SPACE character is allowed for padding.','8 bytes fixed
In the context of a Query with range matching (see PS3.4), the length is 18 bytes maximum.','A string of characters of the format YYYYMMDD; where YYYY shall contain year, MM shall contain the month, and DD shall contain the day, interpreted as a date of the Gregorian calendar system.
Example:
""19930822"" would represent August 22, 1993.
Note

1. The ACR-NEMA Standard 300 (predecessor to DICOM) supported a string of characters of the format YYYY.MM.DD for this VR. Use of this format is not compliant.
2. See also DT VR in this table.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (6,'DS','Decimal String','""0""-""9"", ""+"", ""-"", ""E"", ""e"", ""."" of Default Character Repertoire','16 bytes maximum','A string of characters representing either a fixed point number or a floating point number. A fixed point number shall contain only the characters 0-9 with an optional leading ""+"" or ""-"" and an optional ""."" to mark the decimal point. A floating point number shall be conveyed as defined in ANSI X3.9, with an ""E"" or ""e"" to indicate the start of the exponent. Decimal Strings may be padded with leading or trailing spaces. Embedded spaces are not allowed.
Note

Data Elements with multiple values using this VR may not be properly encoded if Explicit-VR transfer syntax is used and the VL of this attribute exceeds 65534 bytes.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (7,'DT','Date Time','""0""-""9"", ""+"", ""-"", ""."" and the SPACE character of Default Character Repertoire','26 bytes maximum
In the context of a Query with range matching (see PS3.4), the length is 54 bytes maximum.','A concatenated date-time character string in the format:
YYYYMMDDHHMMSS.FFFFFF&ZZXX
The components of this string, from left to right, are YYYY = Year, MM = Month, DD = Day, HH = Hour (range ""00"" - ""23""), MM = Minute (range ""00"" - ""59""), SS = Second (range ""00"" - ""60"").
FFFFFF = Fractional Second contains a fractional part of a second as small as 1 millionth of a second (range ""000000"" - ""999999"").
&ZZXX is an optional suffix for offset from Coordinated Universal Time (UTC), where & = ""+"" or ""-"", and ZZ = Hours and XX = Minutes of offset.
The year, month, and day shall be interpreted as a date of the Gregorian calendar system.
A 24-hour clock is used. Midnight shall be represented by only ""0000"" since ""2400"" would violate the hour range.
The Fractional Second component, if present, shall contain 1 to 6 digits. If Fractional Second is unspecified the preceding ""."" shall not be included. The offset suffix, if present, shall contain 4 digits. The string may be padded with trailing SPACE characters. Leading and embedded spaces are not allowed.
A component that is omitted from the string is termed a null component. Trailing null components of Date Time indicate that the value is not precise to the precision of those components. The YYYY component shall not be null. Non-trailing null components are prohibited. The optional suffix is not considered as a component.
A Date Time value without the optional suffix is interpreted to be in the local time zone of the application creating the Data Element, unless explicitly specified by the Timezone Offset From UTC (0008,0201).
UTC offsets are calculated as ""local time minus UTC"". The offset for a Date Time value in UTC shall be +0000.
Note

1. The range of the offset is -1200 to +1400. The offset for United States Eastern Standard Time is -0500. The offset for Japan Standard Time is +0900.
2. The RFC 2822 use of -0000 as an offset to indicate local time is not allowed.
3. A Date Time value of 195308 means August 1953, not specific to particular day. A Date Time value of 19530827111300.0 means August 27, 1953, 11;13 a.m. accurate to 1/10th second.
4. The Second component may have a value of 60 only for a leap second.
5. The offset may be included regardless of null components; e.g., 2007-0500 is a legal value.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (8,'FL','Floating Point Single','not applicable','4 bytes fixed','Single precision binary floating point number represented in IEEE 754:1985 32-bit Floating Point Number Format.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (9,'FD','Floating Point Double','not applicable','8 bytes fixed','Double precision binary floating point number represented in IEEE 754:1985 64-bit Floating Point Number Format.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (10,'IS','Integer String','""0""-""9"", ""+"", ""-"" of Default Character Repertoire
','12 bytes maximum','A string of characters representing an Integer in base-10 (decimal), shall contain only the characters 0 - 9, with an optional leading ""+"" or ""-"". It may be padded with leading and/or trailing spaces. Embedded spaces are not allowed.
The integer, n, represented shall be in the range:
-231<= n <= (231-1).');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (11,'LO','Long String','Default Character Repertoire and/or as defined by (0008,0005).','64 chars maximum (see Note in Section 6.2)','A character string that may be padded with leading and/or trailing spaces. The character code 5CH (the BACKSLASH "\" in ISO-IR 6) shall not be present, as it is used as the delimiter between values in multiple valued data elements. The string shall not have Control Characters except for ESC.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (12,'LT','Long Text','Default Character Repertoire and/or as defined by (0008,0005).','10240 chars maximum (see Note in Section 6.2)','A character string that may contain one or more paragraphs. It may contain the Graphic Character set and the Control Characters, CR, LF, FF, and ESC. It may be padded with trailing spaces, which may be ignored, but leading spaces are considered to be significant. Data Elements with this VR shall not be multi-valued and therefore character code 5CH (the BACKSLASH "\" in ISO-IR 6) may be used.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (13,'OB','Other Byte String','not applicable','see Transfer Syntax definition','A string of bytes where the encoding of the contents is specified by the negotiated Transfer Syntax. OB is a VR that is insensitive to Little/Big Endian byte ordering (see Section 7.3). The string of bytes shall be padded with a single trailing NULL byte value (00H) when necessary to achieve even length.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (14,'OD','Other Double String','not applicable','(2^32)-8 bytes maximum','A string of 64-bit IEEE 754:1985 floating point words. OD is a VR that requires byte swapping within each 64-bit word when changing between Little Endian and Big Endian byte ordering (see Section 7.3).');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (15,'OF','Other Float String','not applicable','(2^32)-4 bytes maximum','A string of 32-bit IEEE 754:1985 floating point words. OF is a VR that requires byte swapping within each 32-bit word when changing between Little Endian and Big Endian byte ordering (see Section 7.3).');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (16,'OW','Other Word String','not applicable','see Transfer Syntax definition','A string of 16-bit words where the encoding of the contents is specified by the negotiated Transfer Syntax. OW is a VR that requires byte swapping within each word when changing between Little Endian and Big Endian byte ordering (see Section 7.3).');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (17,'PN','Person Name','Default Character Repertoire and/or as defined by (0008,0005) excluding Control Characters LF, FF, and CR but allowing Control Character ESC.','64 chars maximum per component group
(see Note in Section 6.2)','A character string encoded using a 5 component convention. The character code 5CH (the BACKSLASH ""\"" in ISO-IR 6) shall not be present, as it is used as the delimiter between values in multiple valued data elements. The string may be padded with trailing spaces. For human use, the five components in their order of occurrence are: family name complex, given name complex, middle name, name prefix, name suffix.
Note

HL7 prohibits leading spaces within a component; DICOM allows leading and trailing spaces and considers them insignificant.
Any of the five components may be an empty string. The component delimiter shall be the caret ""^"" character (5EH). Delimiters are required for interior null components. Trailing null components and their delimiters may be omitted. Multiple entries are permitted in each component and are encoded as natural text strings, in the format preferred by the named person.
For veterinary use, the first two of the five components in their order of occurrence are: responsible party family name or responsible organization name, patient name. The remaining components are not used and shall not be present.
This group of five components is referred to as a Person Name component group.
For the purpose of writing names in ideographic characters and in phonetic characters, up to 3 groups of components (see Annexes H, I and J) may be used. The delimiter for component groups shall be the equals character ""="" (3DH). The three component groups of components in their order of occurrence are: an alphabetic representation, an ideographic representation, and a phonetic representation.
Any component group may be absent, including the first component group. In this case, the person name may start with one or more ""="" delimiters. Delimiters are required for interior null component groups. Trailing null component groups and their delimiters may be omitted.
Precise semantics are defined for each component group. See Section 6.2.1.2.
For examples and notes, see Section 6.2.1.1.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (18,'SH','Short String','Default Character Repertoire and/or as defined by (0008,0005).','16 chars maximum (see Note in Section 6.2)','A character string that may be padded with leading and/or trailing spaces. The character code 05CH (the BACKSLASH "\" in ISO-IR 6) shall not be present, as it is used as the delimiter between values for multiple data elements. The string shall not have Control Characters except ESC.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (19,'SL','Signed Long','not applicable',' 
4 bytes fixed','Signed binary integer 32 bits long in 2''s complement form.
Represents an integer, n, in the range:
- 231<= n <= 231-1.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (20,'SQ','Sequence of Items','not applicable (see Section 7.5)','not applicable (see Section 7.5)','Value is a Sequence of zero or more Items, as defined in Section 7.5.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (21,'SS','Signed Short','not applicable',' 
2 bytes fixed','Signed binary integer 16 bits long in 2''s complement form. Represents an integer n in the range:
-215<= n <= 215-1.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (22,'ST','Short Text','Default Character Repertoire and/or as defined by (0008,0005).','1024 chars maximum (see Note in Section 6.2)','A character string that may contain one or more paragraphs. It may contain the Graphic Character set and the Control Characters, CR, LF, FF, and ESC. It may be padded with trailing spaces, which may be ignored, but leading spaces are considered to be significant. Data Elements with this VR shall not be multi-valued and therefore character code 5CH (the BACKSLASH "\" in ISO-IR 6) may be used.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (23,'TM','Time','""0""-""9"", ""."" and the SPACE character of Default Character Repertoire
In the context of a Query with range matching (see PS3.4), the character ""-"" is allowed.','16 bytes maximum
In the context of a Query with range matching (see PS3.4), the length is 28 bytes maximum.','A string of characters of the format HHMMSS.FFFFFF; where HH contains hours (range ""00"" - ""23""), MM contains minutes (range ""00"" - ""59""), SS contains seconds (range ""00"" - ""60""), and FFFFFF contains a fractional part of a second as small as 1 millionth of a second (range ""000000"" - ""999999""). A 24-hour clock is used. Midnight shall be represented by only ""0000"" since ""2400"" would violate the hour range. The string may be padded with trailing spaces. Leading and embedded spaces are not allowed.
One or more of the components MM, SS, or FFFFFF may be unspecified as long as every component to the right of an unspecified component is also unspecified, which indicates that the value is not precise to the precision of those unspecified components.
The FFFFFF component, if present, shall contain 1 to 6 digits. If FFFFFF is unspecified the preceding ""."" shall not be included.
Examples:
""070907.0705 "" represents a time of 7 hours, 9 minutes and 7.0705 seconds.
""1010"" represents a time of 10 hours, and 10 minutes.
""021 "" is an invalid value.
Note

The ACR-NEMA Standard 300 (predecessor to DICOM) supported a string of characters of the format HH:MM:SS.frac for this VR. Use of this format is not compliant.
See also DT VR in this table.
The SS component may have a value of 60 only for a leap second.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (24,'UI','Unique Identifier (UID)','"0"-"9", "." of Default Character Repertoire','64 bytes maximum','A character string containing a UID that is used to uniquely identify a wide variety of items. The UID is a series of numeric components separated by the period "." character. If a Value Field containing one or more UIDs is an odd number of bytes in length, the Value Field shall be padded with a single trailing NULL (00H) character to ensure that the Value Field is an even number of bytes in length. See Section 9 and Annex B for a complete specification and examples.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (25,'UL','Unsigned Long','not applicable','4 bytes maximum','Unsigned binary integer 32 bits long. Represents an integer n in the range:
0 <= n < 232.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (26,'UN','Unknown','not applicable','Any length valid for any of the other DICOM Value Representations','A string of bytes where the encoding of the contents is unknown (see Section 6.2.2).');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (27,'US','Unsigned Short','not applicable','2 bytes fixed','Unsigned binary integer 16 bits long. Represents integer n in the range:
0 <= n < 216.');
INSERT INTO bonita.dicom_element_value_representation (dicom_element_value_representation_id, name_short, name_long, character_repertoire, length_of_value, definition) VALUES (28,'UT','Unlimited Text','Default Character Repertoire and/or as defined by (0008,0005).','232-2 bytes maximum
See Note 2','A character string that may contain one or more paragraphs. It may contain the Graphic Character set and the Control Characters, CR, LF, FF, and ESC. It may be padded with trailing spaces, which may be ignored, but leading spaces are considered to be significant. Data Elements with this VR shall not be multi-valued and therefore character code 5CH (the BACKSLASH ""\"" in ISO-IR 6) may be used.'); 
 

/* END INSERT INTO TABLE bonita.dicom_element_value_representation            */
/*----------------------------------------------------------------------------*/


/* END INSERT DATA                                                            */
/******************************************************************************/
  -- ------------------------------------------------------------------------------------------------

CREATE TABLE bonita.trialmetadata_dicom_element (
    trialmetadata_dicom_element_id BIGSERIAL PRIMARY KEY,
    trialmetadata_id BIGINT NOT NULL REFERENCES bonita.trialmetadata(id),
    dicom_element_id BIGINT NOT NULL 
        REFERENCES bonita.dicom_element(dicom_element_id),
    UNIQUE(trialmetadata_id, dicom_element_id)
);
ALTER TABLE bonita.trialmetadata_dicom_element OWNER TO agmednet;

-- ------------------------------------------------------------------------------------------------
ALTER TABLE bonita.baselinedossierdocumentdataxref
  OWNER TO "agmednet";
GRANT ALL ON TABLE bonita.baselinedossierdocumentdataxref TO bonita;
GRANT SELECT ON TABLE bonita.baselinedossierdocumentdataxref TO readonly;
GRANT ALL ON TABLE bonita.baselinedossierdocumentdataxref TO agmednet;--
-- JUDI-5078 - grant privs on hiddenroleontrial to security manager
--   
grant all privileges on table public.hiddenroleontrial to "servicemix-securitymanager";
grant all privileges on table public.hiddenroleontrial to "agmednet";   drop index if exists idx_hidden_role_on_trial_trial_user_role;
drop index if exists idx_hidden_role_on_trial_trial_trialsite_user_role;
create unique index idx_hidden_role_on_trial_trial_user_role on hiddenroleontrial(trial_id,useraccount_id,userrole_id) where trialsite_id is null;
create unique index idx_hidden_role_on_trial_trial_trialsite_user_role on hiddenroleontrial(trial_id,trialsite_id,useraccount_id,userrole_id) where trialsite_id is not null;-- This is a postgres upgrade script to be called by the flywaydb tool
-- Create a new script for each database change
-- Once a script has been applied in production or QA do not edit the script, create a new script.
-- If this would create a lengthy or problematic situation, consult with team for the best solution.
-- Copy this template to create a new upgrade script.
-- The name of the upgrade file should be VYYYYMMdd.HHmmss__upgrade.sql (V20161225.160023__upgrade.sql)
-- remember to commit data changes in the script
-- Where possible, make the script idempotent (can be called repeatedly with the same result)
-- Provide the Jira Issue(s) and author in the comments below

-- Change for Jira Issue:  JUDI-4722
-- Author: gstewart 
  
ALTER TABLE public.study ALTER COLUMN deleted DROP NOT NULL;
-- JUDI-5385 / sub task JUDI-5386
DELETE from rolegrantedtorole where grantedrole_id IN (select id from userrole where name not in ('siteMonitor','siteUser','trialManager','trialSender'));
DELETE FROM hiddenroleontrial WHERE userrole_id IN (select id from userrole where name not in ('siteMonitor','siteUser','trialManager','trialSender'));alter table bonita.baselinedossier DROP CONSTRAINT deleted_must_be_null;DROP TABLE IF EXISTS bonita.dicom_element_private CASCADE;
DROP TABLE IF EXISTS bonita.dicom_element_private_subgroup CASCADE;
DROP TABLE IF EXISTS bonita.dicom_element_public CASCADE;
DROP TABLE IF EXISTS bonita.dicom_element CASCADE;
DROP TABLE IF EXISTS bonita.dicom_element_value_representation CASCADE;
DROP TABLE IF EXISTS bonita.dicom_element_public_type CASCADE;
DROP TABLE IF EXISTS bonita.trialmetadata_dicom_element CASCADE;
DROP TABLE IF EXISTS bonita.dicom_element_type CASCADE;
-- Remove any longitudinaldata without valid study_id(s).
-- This is necessary so that we can create the FK for longitudinaldata to study.
delete from longitudinaldata where study_id is null or study_id not in (select id from study);ALTER TABLE pendingtrialuser ALTER COLUMN reinvite DROP NOT NULL;

ALTER TABLE userrole ALTER COLUMN actionbitmask SET NOT NULL;

-- This constraint comes from Imaging. However, on Imaging Production it has a generated name. We'll be giving it a specific name from now on.
ALTER TABLE longitudinaldata ADD CONSTRAINT longitudinaldata_study_id_fkey FOREIGN KEY (study_id) REFERENCES study(id);ALTER TABLE bonita.formresult ADD COLUMN reason text ;
ALTER TABLE bonita.ecrfresult ADD COLUMN reason text ;
ALTER TABLE bonita.reviewerformresult ADD COLUMN reason text;
UPDATE public.userrole SET productbitmask = 0 WHERE name = 'redactor';

