alter table useraccount alter column password drop default;
alter table useraccount alter column username drop default;
alter table public.usersetting alter COLUMN useraccount_id set not null;
alter table public.usersetting alter COLUMN usersettingtype_id set not null;
alter table public.customsearch alter COLUMN criteria drop default;
alter table public.customsearch alter COLUMN name drop default;
alter table public.trial alter COLUMN locked drop default;
alter table public.notificationlist alter COLUMN notificationsbitmask drop default;
alter table public.trialversion alter COLUMN defaultfortrial drop default;
alter table public.trialversion alter COLUMN trial_id set not null;
alter table public.trialsite alter COLUMN currenttrialversion_id set not null;
alter table public.portalemailtemplate alter COLUMN agmemailtemplate set not null;
alter table public.portalemailtemplate alter COLUMN bodyhtmltemplate set not null;
alter table public.portalemailtemplate alter COLUMN bodymessagetemplate set not null;
alter table public.portalemailtemplate alter COLUMN bodytexttemplate set not null;
alter table public.portalemailtemplate alter COLUMN fromemailaddress set not null;
alter table public.portalemailtemplate alter COLUMN fromname set not null;
alter table public.portalemailtemplate alter COLUMN subject set not null;
alter table public.portalemailtemplate alter COLUMN templatename set not null;
alter table public.portalemailtemplate alter COLUMN userrolebitmask set not null;
-- we did this wrong in prerun, so fix this in post run
update useraccount set emailaddress='stacy.smith@mercyhealth.com' where username='slsmith3';
-- requested by derrick
insert into useraccountuserrolexref(useraccount_id,userrole_id) values ((select id from useraccount where username = 'tendemagadmin'), (select id from userrole where name = 'ctmInboundWebService'));
-- fix admin permission
insert into useraccountuserrolexref values (1,1);
insert into useraccountuserrolexref values (1,2);
