#!/bin/bash
main(){
	for table in `cat dumplist`; do
		time pg_dump -h $1 -U hwang -a -t $table -o -x -O -f dump/$table.sql -v portal_db
		psql -U master -h $2 portal_db < dump/$table.sql 2>&1 | tee step25_$table.log
		if grep ERROR step12_$table.log; then
			exit 1
		fi
	done
}
main $1 $2
