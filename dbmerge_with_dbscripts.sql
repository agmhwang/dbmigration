UPDATE bonita.eventinstance set adjudicationphase=1 where adjudicationphase is NULL;
UPDATE pendingtrialuser set reinvite =false where reinvite is null;
UPDATE trial set requireadduserconfirmation =false where requireadduserconfirmation IS NULL;
UPDATE trial set blockformedits = false where blockformedits IS NULL;
UPDATE trial set informonly =false where informonly IS NULL;
UPDATE trialsite SET defaultfortrial=false WHERE defaultfortrial IS NULL;
UPDATE trialsite SET ctmdeleted =false where ctmdeleted IS NULL;
UPDATE trialsubject SET ctmdeleted =false where ctmdeleted IS NULL;
UPDATE trialsubject SET enabled =false where enabled IS NULL;
UPDATE useraccount SET beentotraining =false where beentotraining IS NULL;
UPDATE useraccount SET loggedintodaatleastonce =false where loggedintodaatleastonce IS NULL;
UPDATE useraccount SET visitedexamlist =false where visitedexamlist IS NULL;

-- 1.7.0 post
update historyevent set username = 'judiadmin' WHERE service = 'JUDI' AND username = 'judi';
update userrole set rolebitmask=8,productbitmask=2,sitebased=false, actionbitmask=16, displayname='Adjudicator' where name='adjudicator';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='authorizedUser' where name='authorizedUser';
update userrole set rolebitmask=32,productbitmask=2,sitebased=false, actionbitmask=16, displayname='Committee Chair' where name='committeeChair';
update userrole set rolebitmask=16,productbitmask=2,sitebased=false, actionbitmask=16, displayname='Committee Member' where name='committeeMember';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='ctmInboundWebService' where name='ctmInboundWebService';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='desktopAgentUser' where name='desktopAgentUser';
update userrole set rolebitmask=1,productbitmask=2,sitebased=false, actionbitmask=16, displayname='Event Coordinator' where name='eventCoordinator';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='hardwareAgentUser' where name='hardwareAgentUser';
update userrole set rolebitmask=64,productbitmask=2,sitebased=true, actionbitmask=16, displayname='Monitor' where name='monitor';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='novartisTest' where name='novartisTest';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=256, displayname='Pacs Admin' where name='pacsAdmin';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='portalUser' where name='portalUser';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='qaTest' where name='qaTest';
update userrole set rolebitmask=0,productbitmask=1,sitebased=false, actionbitmask=1024, displayname='Redactor' where name='redactor';
update userrole set rolebitmask=4,productbitmask=2,sitebased=false, actionbitmask=16, displayname='Reviewer' where name='reviewer';
update userrole set rolebitmask=0,productbitmask=1,sitebased=true, actionbitmask=16, displayname='Site Monitor' where name='siteMonitor';
update userrole set rolebitmask=0,productbitmask=1,sitebased=true, actionbitmask=32, displayname='Site Sender' where name='siteUser';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='sponsor' where name='sponsor';
update userrole set rolebitmask=0,productbitmask=0,sitebased=false, actionbitmask=0, displayname='sysAdmin' where name='sysAdmin';
update userrole set rolebitmask=0,productbitmask=1,sitebased=false, actionbitmask=128, displayname='Trial Admin' where name='trialAdmin';
update userrole set rolebitmask=0,productbitmask=1,sitebased=false, actionbitmask=16, displayname='Trial Manager' where name='trialManager';
update userrole set rolebitmask=0,productbitmask=2,sitebased=true, actionbitmask=32, displayname='Uploader' where name='uploader';

-- 1.9.1
update userrole set actionbitmask=0 where productbitmask=2;

-- REMOVE UPLOADER 
UPDATE clinicaltrialpermission SET actionbitmask = (actionbitmask # 32) WHERE targettrial_id IN (select id from trial where productbitmask & 2 = 2) AND actionbitmask & 32 = 32;
-- REMOVE ALL OTHER ROLES
UPDATE clinicaltrialpermission SET actionbitmask = (actionbitmask # 16) WHERE targettrial_id IN (select id from trial where productbitmask & 2 = 2) AND actionbitmask & 16 = 16;
-- DELETE ALL action is 2
DELETE FROM clinicaltrialpermission WHERE targettrial_id IN (select id from trial where productbitmask & 2 = 2) AND actionbitmask = 2;

CREATE ROLE spring PASSWORD 'spring@gm3dn3t';
GRANT agmednet to spring;
ALTER ROLE spring WITH LOGIN;

--
-- Name: invite_judi_user(text, text, bigint, bigint); Type: FUNCTION; Schema: bonita; Owner: bonita
--

CREATE OR REPLACE FUNCTION invite_judi_user(_username text, _userrole text, _trial_id bigint, _trialsite_id bigint DEFAULT 0) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		myrole RECORD;
		mytrial RECORD;
		mytrialsite RECORD;
		myctp RECORD;
		myutr RECORD;
		myuser RECORD;
		myjudiuser RECORD;
		myjudicontact RECORD;
		myjudirole RECORD;
		myjudimembership RECORD;
		counter int;
	BEGIN
		SELECT * from useraccount where username = _username into myuser;
		SELECT * from userrole where name = _userrole into myrole;
		SELECT * from bonita.role where name ilike _userrole into myjudirole;
		
		IF myuser IS NULL THEN 
			Raise Exception 'No user account found for  %', _username;
			return false;
		END IF;
		
		IF myrole IS NULL THEN 
			Raise Exception 'No user role found for  %', _userrole;
			return false;
		END IF;
		
		IF myjudirole IS NULL THEN 
			Raise Exception 'No bonita.role record fround for %', _userrole;
			return false;
		END IF;
		
		SELECT * from trial where id = _trial_id INTO mytrial;
		IF mytrial IS NULL THEN 
			Raise Exception 'No trial found for  %', _trial_id;
			return false;
		ELSIF mytrial.productbitmask != 2 THEN
			Raise Exception 'This is not a judi trial : %', _trial_id;
			return false;
		END IF;
		
		IF _userrole = 'uploader' THEN
			-- make sure we have a site
			SELECT * from trialsite where id = _trialsite_id INTO mytrialsite;
			IF mytrialsite IS NULL THEN 
				Raise Exception 'No trialsite found for  %', _trialsite_id;
				return false;
			ELSIF mytrialsite.trial_id != mytrial.id THEN
				Raise Exception 'This site is not on this trial! site %, trial %', mytrialsite.name, mytrial.name;
				return false;
			END IF;
			
			-- user needs a record with bit mask of two on this trial
			Raise notice 'SELECT * from clinicaltrialpermission WHERE targetstudy_id IS NULL AND targettrialsite_id IS NULL AND targettrial_id = % AND therecipient_id = % INTO myctp;', mytrial.id,myuser.id;
			SELECT * from clinicaltrialpermission WHERE targetstudy_id IS NULL AND targettrialsite_id IS NULL AND targettrial_id = mytrial.id AND therecipient_id = myuser.id INTO myctp;
			IF myctp IS NULL THEN
				Raise notice 'need to INSERT permission for TRIAL : %', _trial_id;
				INSERT INTO clinicaltrialpermission (id, actionbitmask, targettrial_id, targettrialsite_id, therecipient_id, version) VALUES(nextval('public.hibernate_sequence'),2,mytrial.id,NULL,myuser.id, now());
			ELSE
				Raise notice 'need to update TRIAL permission for ctp ID : %', myctp.id;
				update clinicaltrialpermission set actionbitmask = (actionbitmask | 2) WHERE id = myctp.id;
			END IF;
			
			-- user needs a record with bit mask of 34 on this trialsite
			SELECT * from clinicaltrialpermission WHERE targetstudy_id IS NULL AND targettrialsite_id = mytrialsite.id AND targettrial_id = mytrial.id AND therecipient_id = myuser.id INTO myctp;
			IF myctp IS NULL THEN
				Raise notice 'need to INSERT permission for SITE : %', _trial_id;
				INSERT INTO clinicaltrialpermission (id, actionbitmask, targettrial_id, targettrialsite_id, therecipient_id, version) VALUES(nextval('public.hibernate_sequence'),34,mytrial.id,mytrialsite.id,myuser.id, now());
			ELSE
				Raise notice 'need to update SITE permission for ctp ID : %', myctp.id;
				update clinicaltrialpermission set actionbitmask = (actionbitmask | 34) WHERE id = myctp.id;
			END IF;
		ELSE
			--we're only worried about the trial
			SELECT * from clinicaltrialpermission WHERE targetstudy_id IS NULL AND targettrialsite_id IS NULL AND targettrial_id = mytrial.id AND therecipient_id = myuser.id INTO myctp;
			IF myctp IS NULL THEN
				Raise notice 'need to INSERT permission for TRIAL : %', _trial_id;
				INSERT INTO clinicaltrialpermission (id, actionbitmask, targettrial_id, targettrialsite_id, therecipient_id, version) VALUES(nextval('public.hibernate_sequence'),18,mytrial.id,NULL,myuser.id, now());
			ELSE
				Raise notice 'need to update TRIAL permission for ctp ID : %', myctp.id;
				update clinicaltrialpermission set actionbitmask = (actionbitmask | 18) WHERE id = myctp.id;
			END IF;
		END IF;
		
		-- add the usertrialrolexref record
		SELECT * FROM usertrialrolexref where useraccount_id = myuser.id AND userrole_id = myrole.id AND trial_id = mytrial.id INTO myutr;
		IF myutr IS NULL THEN
			Raise notice 'need to INSERT record into usertrialrolexref ';
			INSERT INTO usertrialrolexref (created,userrole_id,useraccount_id,trial_id) VALUES (now(),myrole.id,myuser.id,mytrial.id);
		ELSE
			Raise notice 'user already has this role on trial';
		END IF;
		
		
		-- does the user exist in JUDI?
		SELECT * FROM bonita.user_ WHERE username = _username INTO myjudiuser;
		IF myjudiuser IS NULL THEN
			Raise notice 'need to INSERT record into bonita.user_ ';
			INSERT INTO bonita.user_ (tenantid,id,username,password,firstname,lastname,manageruserid,createdby,creationdate,lastupdate,enabled) VALUES(1,nextval('public.hibernate_sequence'),myuser.username,'LcTkpvvquKf4KO+prsfXrQ==',myuser.firstname,myuser.lastname,0,-1,1429897677997,1429897677997,true) RETURNING * INTO myjudiuser;
		ELSE
			Raise notice 'user already exists in bonita.user_';
		END IF;
		
		IF myjudiuser IS NULL THEN
			Raise exception 'STILL MISSING judi record';
		END IF;
		
		-- bonita.user_contactinfo (tenantid |    id    | userid,personal) VALUES
		SELECT * FROM bonita.user_contactinfo WHERE userid = myjudiuser.id INTO myjudicontact;
		IF myjudicontact IS NULL THEN
			Raise notice 'need to INSERT record into bonita.user_contactinfo ';
			INSERT INTO bonita.user_contactinfo (tenantid,id,userid,personal) VALUES(1,nextval('public.hibernate_sequence'),myjudiuser.id,true);
		ELSE
			Raise notice 'user already exists in bonita.user_';
		END IF;
		
		--does this user have a membership?
		--  bonita.user_membership (tenantid | id  | userid | roleid | groupid | assignedby | assigneddate) VALUES(1 | 403 |    403 |      4 |       1 |         -1 | 1438020307334
		select * from bonita.user_membership WHERE tenantid = 1 AND userid = myjudiuser.id AND roleid = myjudirole.id INTO myjudimembership;
		IF myjudimembership IS NULL THEN 
			Raise notice 'need to INSERT record into bonita.user_membership';
			INSERT INTO bonita.user_membership(tenantid, id ,userid ,roleid ,groupid, assignedby, assigneddate) VALUES (1,nextval('public.hibernate_sequence'),myjudiuser.id,myjudirole.id,1,-1,1438020307334);
		ELSE
			Raise notice 'record already exists in bonita.user_membership';
		END IF;
		
		RETURN true;
	END;
$$;

--
-- Name: add_column(text, text, text, text); Type: FUNCTION; Schema: public; Owner: master
--

CREATE OR REPLACE FUNCTION add_column(_schema_name text, _table_name text, _column_name text, _data_type text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
  _tmp integer;
BEGIN
execute ' (SELECT count(*) FROM information_schema.columns WHERE table_schema=' || quote_literal(_schema_name) ||  ' and table_name=' || quote_literal(_table_name) || ' and column_name=' || quote_literal(_column_name) || ')'
               into _tmp;
if _tmp = 0 then
	execute 'alter table ' || quote_ident(_schema_name) || '.' || quote_ident(_table_name) || ' add column ' || quote_ident(_column_name) || ' ' || quote_ident(_data_type) || ';';
	end if;

               return _tmp;
END
$$;


--
-- Name: addadditionaldocument(); Type: FUNCTION; Schema: public; Owner: master
--

CREATE OR REPLACE FUNCTION addadditionaldocument() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		eType RECORD;
	BEGIN
		FOR eType IN SELECT * from bonita.eventtype WHERE trial_id = 583742440 LOOP
			RAISE NOTICE 'adding Other document for event % id % on trial %' ,eType.name,eType.id, eType.trial_id;
			INSERT INTO bonita.documenttype(id, version, name, required, sortorder, eventtype_id) VALUES(nextval('public.hibernate_sequence'), now(), 'Other', false, (select max(sortorder) from bonita.documenttype where eventtype_id = eType.id) + 1, eType.id);

		END LOOP;
		return true;
	END;
$$;


--
-- Name: addpatientprofile(); Type: FUNCTION; Schema: public; Owner: master
--

CREATE OR REPLACE FUNCTION addpatientprofile() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
	DECLARE
		eType RECORD;
	BEGIN
		FOR eType IN SELECT * from bonita.eventtype WHERE trial_id = 577993441 or trial_id = 578873438 LOOP
			RAISE NOTICE 'adding Patient Profile for event % id % on trial %' ,eType.name,eType.id, eType.trial_id;
			INSERT INTO bonita.documenttype(id, version, name, required, sortorder, eventtype_id) VALUES(nextval('public.hibernate_sequence'), now(), 'Patient Profile', false, (select max(sortorder) from bonita.documenttype where eventtype_id = eType.id) + 1, eType.id);

		END LOOP;
		return true;
	END;
$$;

--
-- Name: commacat(text, text); Type: FUNCTION; Schema: public; Owner: agmednet
--

CREATE OR REPLACE FUNCTION commacat(acc text, instr text) RETURNS text
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF acc IS NULL OR acc = '' THEN
      RETURN instr;
    ELSE
      RETURN acc || ', ' || instr;
    END IF;
  END;
$$;


--
-- Name: current_database_oid(); Type: FUNCTION; Schema: public; Owner: agmednet
--

CREATE OR REPLACE FUNCTION current_database_oid() RETURNS oid
    LANGUAGE sql IMMUTABLE STRICT
    AS $$ select oid from pg_database where datname = current_database(); $$;

-- baseline dossier
ALTER TABLE bonita.baselinedossier ALTER COLUMN data DROP NOT NULL;
ALTER TABLE bonita.baselinedossier ADD column deleted boolean default false not null;
ALTER TABLE bonita.baselinedossier ADD constraint deleted_must_be_null CHECK ((((data IS NULL) AND (deleted = true)) OR ((data IS NOT NULL) AND (deleted = false))));

-- baselinedossierdocumentdataxref
CREATE TABLE baselinedossierdocumentdataxref (
    id bigint NOT NULL,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    orderby integer NOT NULL,
    updated timestamp without time zone NOT NULL,
    baselinedossier_id bigint NOT NULL,
    documentdata_id bigint NOT NULL
);
ALTER TABLE baselinedossierdocumentdataxref set SCHEMA bonita;
ALTER TABLE bonita.baselinedossierdocumentdataxref OWNER TO "servicemix-adjudication";

-- document
ALTER TABLE bonita.document ADD column deleted boolean not null default false;
ALTER TABLE bonita.document ALTER COLUMN deleted drop default;
ALTER TABLE bonita.document ADD column flaggedfortranslation timestamp without time zone;
ALTER TABLE bonita.document ADD column translationcanceled timestamp without time zone;

-- documentdata
ALTER TABLE bonita.documentdata DROP COLUMN istranslation ;
ALTER TABLE bonita.documentdata DROP COLUMN uploadcomplete;
ALTER TABLE bonita.documentdata ADD COLUMN uploadcomplete boolean;
ALTER TABLE bonita.documentdata ADD COLUMN thumbnaildata bytea;
ALTER TABLE bonita.documentdata ADD COLUMN thumbnailheight integer;
ALTER TABLE bonita.documentdata ADD COLUMN thumbnailwidth integer;

-- eventaccessuseraccount
CREATE TABLE eventaccessuseraccount (
    id bigint NOT NULL,
    version timestamp without time zone,
    assignedfrom text NOT NULL,
    completed boolean NOT NULL,
    eventinstance_id bigint NOT NULL,
    useraccount_id bigint NOT NULL,
    userrole_id bigint NOT NULL
);
ALTER TABLE eventaccessuseraccount OWNER TO bonita;
ALTER TABLE eventaccessuseraccount set SCHEMA bonita;

-- eventaccessuserrole
CREATE TABLE eventaccessuserrole (
    id bigint NOT NULL,
    version timestamp without time zone,
    eventinstance_id bigint NOT NULL,
    userrole_id bigint NOT NULL
);
ALTER TABLE eventaccessuserrole OWNER TO bonita;
ALTER TABLE eventaccessuserrole set SCHEMA bonita;

-- eventinstance
ALTER TABLE bonita.eventinstance ALTER COLUMN adjudicationphase set DEFAULT 1;
ALTER TABLE bonita.eventinstance ALTER COLUMN adjudicationphase set NOT NULL;
ALTER TABLE bonita.eventinstance ADD column reviewphase integer DEFAULT 1;
ALTER TABLE bonita.eventinstance ADD column uploadsdisabled boolean NOT NULL default false;
ALTER TABLE bonita.eventinstance ALTER COLUMN uploadsdisabled DROP DEFAULT;
ALTER TABLE bonita.eventinstance ADD column timescompleted integer DEFAULT 0 NOT NULL;
ALTER TABLE bonita.eventinstance ADD column migrated boolean;
ALTER TABLE bonita.eventinstance ADD column state text;

-- eventtype
ALTER TABLE bonita.eventtype ADD column trial_id bigint;
UPDATE bonita.eventtype set trial_id =bonita.eventinstance.trial_id FROM bonita.eventinstance WHERE bonita.eventtype.id=bonita.eventinstance.eventtype_id;
DELETE FROM bonita.ecrf where eventtype_id IN (468943374,468943375,468943376,468943377);
delete from bonita.documenttype where eventtype_id IN (468943374,468943375,468943376,468943377);
DELETE FROM bonita.eventtype where trial_id is NULL;
ALTER TABLE bonita.eventtype ALTER COLUMN trial_id set not null;

-- eventtypestate
CREATE TABLE eventtypestate (
    id bigint NOT NULL,
    version timestamp without time zone,
    state text,
    xml text,
    eventtype_id bigint NOT NULL
);
ALTER TABLE eventtypestate OWNER TO bonita;
ALTER TABLE eventtypestate set SCHEMA bonita;

-- finaloutcome
CREATE TABLE finaloutcome (
    id bigint NOT NULL,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    xml text NOT NULL,
    type text NOT NULL,
    versionnumber integer NOT NULL,
    eventinstance_id bigint NOT NULL,
    userrole_id bigint NOT NULL,
    CONSTRAINT finaloutcome_versionnumber_check CHECK ((versionnumber > 0))
);
ALTER TABLE finaloutcome OWNER TO bonita;
ALTER TABLE finaloutcome set SCHEMA bonita;

-- form
CREATE TABLE form (
    id bigint NOT NULL,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    xml text NOT NULL,
    name text NOT NULL,
    trial_id bigint NOT NULL
);
ALTER TABLE form OWNER TO bonita;
ALTER TABLE form set SCHEMA bonita;

-- formresult
CREATE TABLE formresult (
    id bigint NOT NULL,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    xml text NOT NULL,
    form_id bigint NOT NULL,
    eventinstance_id bigint NOT NULL,
    useraccount_id bigint NOT NULL,
    userrole_id bigint NOT NULL,
    phase integer DEFAULT 1 NOT NULL,
    document_id bigint,
    disabled boolean DEFAULT false NOT NULL,
    inprogress boolean DEFAULT false NOT NULL,
    CONSTRAINT inprogressanddisabled CHECK (((inprogress = false) OR ((inprogress = true) AND (disabled = true))))
);
ALTER TABLE formresult OWNER TO bonita;
ALTER TABLE formresult set SCHEMA bonita;

-- query
ALTER TABLE bonita.query ADD column startuserrole_id bigint;
ALTER TABLE bonita.query ADD column deleted boolean default false not null;
ALTER TABLE bonita.query ALTER COLUMN deleted DROP DEFAULT;

-- reviewerform
ALTER TABLE bonita.reviewerform DROP COLUMN eventtype_id;

-- trialeventtypexref
DROP TABLE bonita.trialeventtypexref;

-- trialxml
CREATE TABLE trialxml (
    id bigint NOT NULL,
    version timestamp without time zone,
    xml text,
    trial_id bigint,
    xmltype_id bigint
);
ALTER TABLE trialxml OWNER TO bonita;
ALTER TABLE trialxml SET SCHEMA bonita;

-- xmltype
CREATE TABLE xmltype (
    id bigint NOT NULL,
    version timestamp without time zone,
    name text
);
ALTER TABLE xmltype OWNER TO bonita;
ALTER TABLE xmltype SET SCHEMA bonita;

-- pendingtrialuser
ALTER TABLE pendingtrialuser ALTER COLUMN reinvite set DEFAULT false;
ALTER TABLE pendingtrialuser ALTER COLUMN reinvite set NOT NULL;

-- portalemaillog
ALTER TABLE portalemaillog ALTER COLUMN dismissed set DEFAULT false;
ALTER TABLE portalemaillog ALTER COLUMN sent set DEFAULT false;

-- portalemailtemplate
ALTER TABLE portalemailtemplate ADD column trial_id bigint;

-- report
CREATE TABLE report (
    id bigint NOT NULL,
    name text,
    compiled bytea
);
ALTER TABLE report OWNER TO agmednet;

-- reportformat
CREATE TABLE reportformat (
    id bigint NOT NULL,
    name text
);
ALTER TABLE reportformat OWNER TO agmednet;

-- reportformattrialxref
CREATE TABLE reportformattrialxref (
    id bigint NOT NULL,
    report_id bigint NOT NULL,
    reportformat_id bigint NOT NULL,
    trial_id bigint NOT NULL
);
ALTER TABLE reportformattrialxref OWNER TO agmednet;

-- view status
drop VIEW status;

-- study
ALTER TABLE study ALTER COLUMN deleted DROP DEFAULT;
ALTER TABLE study ALTER COLUMN deleted DROP NOT NULL;
ALTER TABLE study ALTER COLUMN finaluse DROP DEFAULT;

-- trial
ALTER TABLE trial ALTER COLUMN requireadduserconfirmation SET DEFAULT false;
ALTER TABLE trial ALTER COLUMN requireadduserconfirmation SET NOT NULL;
ALTER TABLE trial ALTER COLUMN blockformedits SET DEFAULT false;
ALTER TABLE trial ALTER COLUMN blockformedits SET NOT NULL;
ALTER TABLE trial ALTER COLUMN cloudenabled SET DEFAULT false;
ALTER TABLE trial ALTER COLUMN cloudenabled SET NOT NULL;
ALTER TABLE trial ALTER COLUMN informonly SET DEFAULT false;
ALTER TABLE trial ALTER COLUMN informonly SET NOT NULL;
ALTER TABLE trial ALTER COLUMN hasredactabledocuments SET DEFAULT false;
ALTER TABLE trial ALTER COLUMN hasredactabledocuments SET NOT NULL;
ALTER TABLE trial ADD column protocolconfirmationrequired boolean DEFAULT false NOT NULL;
ALTER TABLE trial ADD column protocolconfirmationtext text;
ALTER TABLE trial ADD column trialcharterlink text;
ALTER TABLE trial ADD constraint protocol_message_required CHECK (((protocolconfirmationrequired = false) OR ((protocolconfirmationrequired = true) AND (protocolconfirmationtext IS NOT NULL))));

-- trialsite
ALTER TABLE trialsite ALTER COLUMN enabled SET DEFAULT false;
ALTER TABLE trialsite ALTER COLUMN telerad SET DEFAULT false;
ALTER TABLE trialsite ALTER COLUMN telerad SET NOT NULL;
ALTER TABLE trialsite ALTER COLUMN defaultfortrial SET DEFAULT false;
ALTER TABLE trialsite ALTER COLUMN defaultfortrial SET NOT NULL;
ALTER TABLE trialsite ALTER COLUMN ctmdeleted SET DEFAULT false;
ALTER TABLE trialsite ALTER COLUMN ctmdeleted SET NOT NULL;

-- trialsubject
ALTER TABLE trialsubject ALTER COLUMN ctmdeleted SET DEFAULT false;
ALTER TABLE trialsubject ALTER COLUMN ctmdeleted SET NOT NULL;
ALTER TABLE trialsubject ALTER COLUMN enabled SET DEFAULT false;
ALTER TABLE trialsubject ALTER COLUMN enabled SET NOT NULL;

-- useraccount
ALTER TABLE useraccount ALTER COLUMN enabled set DEFAULT false;
ALTER TABLE useraccount ALTER COLUMN beentotraining set DEFAULT false;
ALTER TABLE useraccount ALTER COLUMN beentotraining set not null;
ALTER TABLE useraccount ALTER COLUMN loggedintodaatleastonce set DEFAULT false;
ALTER TABLE useraccount ALTER COLUMN loggedintodaatleastonce set not null;
ALTER TABLE useraccount ALTER COLUMN visitedexamlist set DEFAULT false;
ALTER TABLE useraccount ALTER COLUMN visitedexamlist set not null;

-- userrole
ALTER TABLE userrole ALTER COLUMN actionbitmask SET DEFAULT 0;
ALTER TABLE userrole ALTER COLUMN displayname SET NOT NULL;
ALTER TABLE userrole ALTER COLUMN sitebased SET DEFAULT false;
ALTER TABLE userrole DROP COLUMN trial;
ALTER TABLE userrole ADD column trial_id bigint;
ALTER TABLE userrole ADD constraint customrole_display_name_equals_name CHECK (((name = displayname) OR (trial_id IS NULL)));

-- userroleontrial
CREATE TABLE usersroleontrial (
    id bigint NOT NULL,
    version timestamp without time zone,
    trial_id bigint NOT NULL,
    trialsite_id bigint,
    useraccount_id bigint NOT NULL,
    userrole_id bigint NOT NULL
);
ALTER TABLE usersroleontrial OWNER TO agmednet;

-- JUDI ROLES
--insert all non site-based judi roles
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id)(SELECT nextval('public.hibernate_sequence'),created,trial_id,useraccount_id,userrole_id FROM usertrialrolexref WHERE userrole_id NOT IN (select id from userrole where name in ('monitor','uploader')));

--insert uploader
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (select nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='uploader'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NOT NULL AND targettrial_id IN (select id from trial where productbitmask &2 =2 ) AND actionbitmask & 32 = 32 AND therecipient_id IN (select distinct useraccount_id FROM usertrialrolexref WHERE userrole_id = (select id from userrole where name ='uploader')) AND targettrialsite_id IN (select id from trialsite));

--insert monitor
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (select nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='monitor'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NOT NULL AND targettrial_id IN (select id from trial where productbitmask &2 =2 ) AND actionbitmask & 16 = 16 AND therecipient_id IN (select distinct useraccount_id FROM usertrialrolexref WHERE userrole_id = (select id from userrole where name ='monitor')) AND targettrialsite_id IN (select id from trialsite));

--IMAGING ROLES
-- NOTE : if you run into insert errors, it's probably because clinical trialpermission had duplicate data, you can search the offending records by querying :
-- select * from clinicaltrialpermission where therecipient_id = XX AND targetstudy_id IS NULL;.. you will probably be able to see the problem right off the bat

-- siteUser
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (SELECT DISTINCT ON (targettrial_id,therecipient_id, targettrialsite_id) nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='siteUser'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NOT NULL AND targettrial_id IN (select id from trial where productbitmask &1 =1 ) AND actionbitmask & 32 = 32 AND therecipient_id IN (select distinct useraccount_id from useraccountuserrolexref WHERE userrole_id = (select id from userrole where name ='siteUser')) AND targettrialsite_id IN (select distinct id from trialsite));
-- siteMonitor
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (select DISTINCT ON (targettrial_id,therecipient_id, targettrialsite_id) nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='siteMonitor'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NOT NULL AND targettrial_id IN (select id from trial where productbitmask &1 =1 ) AND actionbitmask & 16 = 16 AND therecipient_id IN (select distinct useraccount_id from useraccountuserrolexref WHERE userrole_id = (select id from userrole where name ='siteMonitor'))AND targettrialsite_id IN (select distinct id from trialsite));
--trialManager
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (select DISTINCT ON (targettrial_id,therecipient_id) nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='trialManager'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NULL AND targettrial_id IN (select id from trial where productbitmask &1 =1 ) AND actionbitmask & 16 = 16 AND therecipient_id IN (select distinct useraccount_id from useraccountuserrolexref WHERE userrole_id = (select id from userrole where name ='trialManager')));
--trialAdmin
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (select DISTINCT ON (targettrial_id,therecipient_id) nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='trialAdmin'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NULL AND targettrial_id IN (select id from trial where productbitmask &1 =1 ) AND actionbitmask & 128 = 128 AND therecipient_id IN (select distinct useraccount_id from useraccountuserrolexref WHERE userrole_id = (select id from userrole where name ='trialAdmin')));
--redactor
INSERT INTO usersroleontrial (id,version,trial_id,useraccount_id,userrole_id,trialsite_id) (select DISTINCT ON (targettrial_id,therecipient_id) nextval('public.hibernate_sequence'),version,targettrial_id,therecipient_id,(select id from userrole where name ='redactor'),targettrialsite_id from clinicaltrialpermission where targetstudy_id IS NULL AND targettrialsite_id IS NULL AND targettrial_id IN (select id from trial where productbitmask &1 =1 ) AND actionbitmask & 1024 = 1024 AND therecipient_id IN (select distinct useraccount_id from useraccountuserrolexref WHERE userrole_id = (select id from userrole where name ='redactor')));

-- usertrialprotocoltraining
CREATE TABLE usertrialprotocoltraining (
    id bigint NOT NULL,
    version timestamp without time zone,
    trial_id bigint NOT NULL,
    useraccount_id bigint NOT NULL
);
ALTER TABLE usertrialprotocoltraining OWNER TO agmednet;

-- baselinedossier index
--
-- Name: baselinedossierdocumentdataxr_baselinedossier_id_documentda_key; Type: CONSTRAINT; Schema: bonita; Owner: servicemix-adjudication; Tablespace: 
--

ALTER TABLE ONLY bonita.baselinedossierdocumentdataxref
    ADD CONSTRAINT baselinedossierdocumentdataxr_baselinedossier_id_documentda_key UNIQUE (baselinedossier_id, documentdata_id);


--
-- Name: baselinedossierdocumentdataxref_baselinedossier_id_orderby_key; Type: CONSTRAINT; Schema: bonita; Owner: servicemix-adjudication; Tablespace: 
--

ALTER TABLE ONLY bonita.baselinedossierdocumentdataxref
    ADD CONSTRAINT baselinedossierdocumentdataxref_baselinedossier_id_orderby_key UNIQUE (baselinedossier_id, orderby);


--
-- Name: baselinedossierdocumentdataxref_pkey; Type: CONSTRAINT; Schema: bonita; Owner: servicemix-adjudication; Tablespace: 
--

ALTER TABLE ONLY bonita.baselinedossierdocumentdataxref
    ADD CONSTRAINT baselinedossierdocumentdataxref_pkey PRIMARY KEY (id);


-- eventinstance 
ALTER TABLE bonita.eventinstance DROP CONSTRAINT eventinstance_aeid_trial_id_key;

-- eventaccess*
ALTER TABLE ONLY bonita.eventaccessuseraccount

    ADD CONSTRAINT eventaccessuseraccount_pkey PRIMARY KEY (id);


--
-- Name: eventaccessuseraccount_unique_eventRoleAssignedUser; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.eventaccessuseraccount
    ADD CONSTRAINT "eventaccessuseraccount_unique_eventRoleAssignedUser" UNIQUE (eventinstance_id, userrole_id, assignedfrom, useraccount_id);


--
-- Name: eventaccessuserrole_pkey; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.eventaccessuserrole
    ADD CONSTRAINT eventaccessuserrole_pkey PRIMARY KEY (id);


--
-- Name: eventaccessuserrole_unique_eventinstance_id_userrole_id; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.eventaccessuserrole
    ADD CONSTRAINT eventaccessuserrole_unique_eventinstance_id_userrole_id UNIQUE (eventinstance_id, userrole_id);

-- eventtype
ALTER TABLE bonita.eventtype DROP CONSTRAINT eventtype_name_processdefinition_id_key;

-- eventtypestate
ALTER TABLE ONLY bonita.eventtypestate
    ADD CONSTRAINT eventtypestate_eventtype_id_state_key UNIQUE (eventtype_id, state);


--
-- Name: eventtypestate_pkey; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.eventtypestate
    ADD CONSTRAINT eventtypestate_pkey PRIMARY KEY (id);

-- finaloutcome
ALTER TABLE ONLY bonita.finaloutcome
    ADD CONSTRAINT finaloutcome_pkey PRIMARY KEY (id);

-- form*
ALTER TABLE ONLY bonita.form
    ADD CONSTRAINT form_pkey PRIMARY KEY (id);


--
-- Name: formresult_pkey; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.formresult
    ADD CONSTRAINT formresult_pkey PRIMARY KEY (id);

-- reviewerform
ALTER TABLE ONLY bonita.reviewerform
    ADD CONSTRAINT reviewerform_trial_id_key UNIQUE (trial_id);

-- trialxml
ALTER TABLE ONLY bonita.trialxml
    ADD CONSTRAINT trialxml_pkey PRIMARY KEY (id);


--
-- Name: trialxml_trial_id_xmltype_id_key; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.trialxml
    ADD CONSTRAINT trialxml_trial_id_xmltype_id_key UNIQUE (trial_id, xmltype_id);

-- xmltype
ALTER TABLE ONLY bonita.xmltype
    ADD CONSTRAINT xmltype_name_key UNIQUE (name);


--
-- Name: xmltype_pkey; Type: CONSTRAINT; Schema: bonita; Owner: bonita; Tablespace:
--

ALTER TABLE ONLY bonita.xmltype
    ADD CONSTRAINT xmltype_pkey PRIMARY KEY (id);

-- userrole
ALTER TABLE ONLY userrole
    ADD CONSTRAINT "RoleIsUniqueWithinTrial" UNIQUE (trial_id, name);
ALTER TABLE userrole DROP CONSTRAINT userrole_name_key;

-- report*
ALTER TABLE ONLY report
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: reportformat_pkey; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY reportformat
    ADD CONSTRAINT reportformat_pkey PRIMARY KEY (id);


--
-- Name: reportformatname_key; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY reportformat
    ADD CONSTRAINT reportformatname_key UNIQUE (name);


--
-- Name: reportname_key; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY report
    ADD CONSTRAINT reportname_key UNIQUE (name);


--
-- Name: reportreportformatxref_key; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY reportformattrialxref
    ADD CONSTRAINT reportreportformatxref_key UNIQUE (trial_id, report_id, reportformat_id);


--
-- Name: reportreportformatxref_pkey; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY reportformattrialxref
    ADD CONSTRAINT reportreportformatxref_pkey PRIMARY KEY (id);

-- usersrole
ALTER TABLE ONLY usersroleontrial

    ADD CONSTRAINT usersroleontrial_pkey PRIMARY KEY (id);


--
-- Name: usertrialprotocoltraining_pkey; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY usertrialprotocoltraining
    ADD CONSTRAINT usertrialprotocoltraining_pkey PRIMARY KEY (useraccount_id, trial_id);


--
-- Name: usertrialprotocoltraining_trial_id_useraccount_id_key; Type: CONSTRAINT; Schema: public; Owner: agmednet; Tablespace: 
--

ALTER TABLE ONLY usertrialprotocoltraining
    ADD CONSTRAINT usertrialprotocoltraining_trial_id_useraccount_id_key UNIQUE (trial_id, useraccount_id);

-- document index
drop INDEX bonita.document_documenttype_instance;
CREATE UNIQUE INDEX document_documenttype_instance_istranslation ON bonita.document USING btree (eventinstance_id, documenttype_id, instance, istranslation);

-- documenttype
CREATE UNIQUE INDEX documenttype_name_eventtype_id_key ON bonita.documenttype USING btree (name, eventtype_id);

-- ecrfresult
DROP INDEX bonita.ecrfresult_user_no_phase;
DROP INDEX bonita.ecrfresult_user_phase;
CREATE UNIQUE INDEX ecrfresult_user_phase_role ON bonita.ecrfresult USING btree (eventinstance_id, useraccount_id, phase, userrole_id);


--
-- Name: eventinstance_aeid_trial_id_key; Type: INDEX; Schema: bonita; Owner: bonita; Tablespace: 
--

CREATE UNIQUE INDEX eventinstance_aeid_trial_id_key ON bonita.eventinstance USING btree (aeid, trial_id) WHERE (disabled = false);


--
-- Name: eventyype_name_per_trial; Type: INDEX; Schema: bonita; Owner: bonita; Tablespace: 
--

CREATE UNIQUE INDEX eventyype_name_per_trial ON bonita.eventtype USING btree (name, trial_id);


--
-- Name: finaloutcome_unique_versionnumber_per_event; Type: INDEX; Schema: bonita; Owner: bonita; Tablespace: 
--

CREATE UNIQUE INDEX finaloutcome_unique_versionnumber_per_event ON bonita.finaloutcome USING btree (eventinstance_id, versionnumber);


--
-- Name: form_unique_name_per_trial; Type: INDEX; Schema: bonita; Owner: bonita; Tablespace: 
--

CREATE UNIQUE INDEX form_unique_name_per_trial ON bonita.form USING btree (trial_id, name);


--
-- Name: formresult_unique_submission_no_document; Type: INDEX; Schema: bonita; Owner: bonita; Tablespace: 
--

CREATE UNIQUE INDEX formresult_unique_submission_no_document ON bonita.formresult USING btree (form_id, eventinstance_id, useraccount_id, userrole_id, phase) WHERE (document_id IS NULL);


--
-- Name: formresult_unique_submission_with_document; Type: INDEX; Schema: bonita; Owner: bonita; Tablespace: 
--

CREATE UNIQUE INDEX formresult_unique_submission_with_document ON bonita.formresult USING btree (form_id, eventinstance_id, useraccount_id, userrole_id, phase, document_id) WHERE (document_id IS NOT NULL);

-- trigger on process instance
DROP TRIGGER delete_process_instance on bonita.process_instance;

-- userroleontrial
CREATE UNIQUE INDEX usersroleontrial_trial_no_site_user_role ON usersroleontrial USING btree (trial_id, useraccount_id, userrole_id) WHERE (trialsite_id IS NULL);



--
-- Name: usersroleontrial_trial_site_user_role; Type: INDEX; Schema: public; Owner: agmednet; Tablespace: 
--

CREATE UNIQUE INDEX usersroleontrial_trial_site_user_role ON usersroleontrial USING btree (trial_id, trialsite_id, useraccount_id, userrole_id) WHERE (trialsite_id IS NOT NULL);

-- index
ALTER TABLE ONLY bonita.finaloutcome
    ADD CONSTRAINT finaloutcome_eventinstance_id_fkey FOREIGN KEY (eventinstance_id) REFERENCES bonita.eventinstance(id);


--
-- Name: finaloutcome_userrole_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY bonita.finaloutcome
    ADD CONSTRAINT finaloutcome_userrole_id_fkey FOREIGN KEY (userrole_id) REFERENCES public.userrole(id);


--
-- Name: fk1ee24d48b87f3e1; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY bonita.eventtype
    ADD CONSTRAINT fk1ee24d48b87f3e1 FOREIGN KEY (trial_id) REFERENCES public.trial(id);

ALTER TABLE ONLY bonita.eventaccessuseraccount
    ADD CONSTRAINT fk3bd46cc46fbe62f3 FOREIGN KEY (userrole_id) REFERENCES public.userrole(id);


--
-- Name: fk3bd46cc481fc1e61; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY bonita.eventaccessuseraccount
    ADD CONSTRAINT fk3bd46cc481fc1e61 FOREIGN KEY (eventinstance_id) REFERENCES bonita.eventinstance(id);


--
-- Name: fk3bd46cc488180b01; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY bonita.eventaccessuseraccount
    ADD CONSTRAINT fk3bd46cc488180b01 FOREIGN KEY (useraccount_id) REFERENCES public.useraccount(id);

ALTER TABLE ONLY bonita.trialxml
    ADD CONSTRAINT fk5945614183d80861 FOREIGN KEY (xmltype_id) REFERENCES bonita.xmltype(id);


--
-- Name: fk594561418b87f3e1; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY bonita.trialxml
    ADD CONSTRAINT fk594561418b87f3e1 FOREIGN KEY (trial_id) REFERENCES public.trial(id);

ALTER TABLE ONLY bonita.eventaccessuserrole
    ADD CONSTRAINT fk81e7d3f6fbe62f3 FOREIGN KEY (userrole_id) REFERENCES public.userrole(id);


--
-- Name: fk81e7d3f81fc1e61; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY bonita.eventaccessuserrole
    ADD CONSTRAINT fk81e7d3f81fc1e61 FOREIGN KEY (eventinstance_id) REFERENCES bonita.eventinstance(id);

SET search_path = bonita, pg_catalog;
ALTER TABLE ONLY baselinedossierdocumentdataxref
    ADD CONSTRAINT fkdb5b3ac63b69dff3 FOREIGN KEY (documentdata_id) REFERENCES documentdata(id);


--
-- Name: fkdb5b3ac6c79f8501; Type: FK CONSTRAINT; Schema: bonita; Owner: servicemix-adjudication
--

ALTER TABLE ONLY baselinedossierdocumentdataxref
    ADD CONSTRAINT fkdb5b3ac6c79f8501 FOREIGN KEY (baselinedossier_id) REFERENCES baselinedossier(id);


--
-- Name: fke196843d57fa5bc1; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY eventtypestate
    ADD CONSTRAINT fke196843d57fa5bc1 FOREIGN KEY (eventtype_id) REFERENCES eventtype(id);


--
-- Name: form_trial_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY form
    ADD CONSTRAINT form_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES public.trial(id);


--
-- Name: formresult_document_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY formresult
    ADD CONSTRAINT formresult_document_id_fkey FOREIGN KEY (document_id) REFERENCES document(id);


--
-- Name: formresult_eventinstance_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY formresult
    ADD CONSTRAINT formresult_eventinstance_id_fkey FOREIGN KEY (eventinstance_id) REFERENCES eventinstance(id);


--
-- Name: formresult_form_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY formresult
    ADD CONSTRAINT formresult_form_id_fkey FOREIGN KEY (form_id) REFERENCES form(id);


--
-- Name: formresult_useraccount_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY formresult
    ADD CONSTRAINT formresult_useraccount_id_fkey FOREIGN KEY (useraccount_id) REFERENCES public.useraccount(id);


--
-- Name: formresult_userrole_id_fkey; Type: FK CONSTRAINT; Schema: bonita; Owner: bonita
--

ALTER TABLE ONLY formresult
    ADD CONSTRAINT formresult_userrole_id_fkey FOREIGN KEY (userrole_id) REFERENCES public.userrole(id);

ALTER TABLE ONLY query
    ADD CONSTRAINT query_startuserrole_id_fkey FOREIGN KEY (startuserrole_id) REFERENCES public.userrole(id);


SET search_path = public, pg_catalog;

ALTER TABLE ONLY trial
    ADD CONSTRAINT fk4d51ef626c7eba4 FOREIGN KEY (daconfigattachment_id) REFERENCES attachment(id);
ALTER TABLE ONLY trial
    ADD CONSTRAINT fk_trialdaform_attachment FOREIGN KEY (daformattachment_id) REFERENCES attachment(id);
ALTER TABLE ONLY usersroleontrial
    ADD CONSTRAINT fkb167e05910d34edd FOREIGN KEY (useraccount_id) REFERENCES useraccount(id);


--
-- Name: fkb167e05938a4cbd; Type: FK CONSTRAINT; Schema: public; Owner: agmednet
--

ALTER TABLE ONLY usersroleontrial
    ADD CONSTRAINT fkb167e05938a4cbd FOREIGN KEY (trial_id) REFERENCES trial(id);


--
-- Name: fkb167e0598b206dfd; Type: FK CONSTRAINT; Schema: public; Owner: agmednet
--

ALTER TABLE ONLY usersroleontrial
    ADD CONSTRAINT fkb167e0598b206dfd FOREIGN KEY (trialsite_id) REFERENCES trialsite(id);


--
-- Name: fkb167e0598e10c97; Type: FK CONSTRAINT; Schema: public; Owner: agmednet
--

ALTER TABLE ONLY usersroleontrial
    ADD CONSTRAINT fkb167e0598e10c97 FOREIGN KEY (userrole_id) REFERENCES userrole(id);

ALTER TABLE ONLY usertrialprotocoltraining
    ADD CONSTRAINT fkea66939d88180b01 FOREIGN KEY (useraccount_id) REFERENCES useraccount(id);


--
-- Name: fkea66939d8b87f3e1; Type: FK CONSTRAINT; Schema: public; Owner: agmednet
--

ALTER TABLE ONLY usertrialprotocoltraining
    ADD CONSTRAINT fkea66939d8b87f3e1 FOREIGN KEY (trial_id) REFERENCES trial(id);


--
-- Name: fkf3f767018b87f3e1; Type: FK CONSTRAINT; Schema: public; Owner: agmednet
--

ALTER TABLE ONLY userrole
    ADD CONSTRAINT fkf3f767018b87f3e1 FOREIGN KEY (trial_id) REFERENCES trial(id);

REVOKE ALL ON SCHEMA bonita FROM PUBLIC;
REVOKE ALL ON SCHEMA bonita FROM bonita;
GRANT ALL ON SCHEMA bonita TO bonita;
GRANT ALL ON SCHEMA bonita TO agmednet;
GRANT USAGE ON SCHEMA bonita TO readonlyuser;


--Added 17-JUN-2016
CREATE TABLE bonita.trialmetadata (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    trial_id BIGINT NOT NULL REFERENCES trial(id),
    eventtype_id BIGINT NOT NULL REFERENCES bonita.eventtype(id),
	name text NOT NULL,
	regex text,
	guidancetext text,
	required BOOLEAN NOT NULL DEFAULT false,
	isdate BOOLEAN NOT NULL DEFAULT false
);
ALTER TABLE bonita.trialmetadata OWNER TO bonita;
CREATE UNIQUE INDEX "trialmetadata_unique_name_trial" ON bonita.trialmetadata(trial_id,name) WHERE eventtype_id IS NULL;
CREATE UNIQUE INDEX "trialmetadata_unique_name_event" ON bonita.trialmetadata(eventtype_id,name) WHERE eventtype_id IS NOT NULL;


CREATE TABLE bonita.eventmetadata (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    eventinstance_id BIGINT NOT NULL REFERENCES bonita.eventinstance(id),
    trialmetadata_id BIGINT NOT NULL REFERENCES bonita.trialmetadata(id),
    value text,
	datevalue DATE
	CHECK ((value IS NOT NULL AND datevalue IS NULL) OR (datevalue IS NOT NULL AND value IS NULL))
);
ALTER TABLE bonita.eventmetadata OWNER TO bonita;
CREATE UNIQUE INDEX "eventmetadata_unique_key_per_event" ON bonita.eventmetadata(eventinstance_id,trialmetadata_id);

-- ADDED 20-JUN-2016.. fixing not null issues with spring code
UPDATE trial SET telerad = false WHERE telerad is null;
ALTER TABLE trial ALTER COLUMN telerad SET NOT NULL;
ALTER TABLE trial ALTER COLUMN telerad SET DEFAULT false;

-- ADDED 20-JUN-2016.. this should have been added to 1.9.1 deployment
ALTER TABLE userrole DROP CONSTRAINT IF EXISTS "userrole_name_key";

-- ADDED 21-JUN-2016
ALTER TABLE bonita.trialmetadata DROP COLUMN eventtype_id ;
-- ADDED 22-JUN-2016
CREATE UNIQUE INDEX "trialmetadata_unique_name_trial" ON bonita.trialmetadata(trial_id,name);
ALTER TABLE bonita.trialmetadata ADD COLUMN sortorder INTEGER NOT NULL UNIQUE ;
-- grant spring permission to the bonita tables
GRANT bonita TO spring;


-- add created and updated columns to judi tables to use the new BaseEntityCreatedUpdated
ALTER TABLE bonita.querycomment ADD COLUMN updated timestamp;
UPDATE bonita.querycomment set updated = created;
ALTER TABLE bonita.querycomment ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.querycomment ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.dossierview ADD COLUMN updated timestamp;
UPDATE bonita.dossierview set updated = created;
ALTER TABLE bonita.dossierview ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.dossierview ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.documentdata ADD COLUMN created timestamp;
UPDATE bonita.documentdata set created = version;
UPDATE bonita.documentdata set created = now() WHERE created is null;
ALTER TABLE bonita.documentdata ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.documentdata ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.documentdata ADD COLUMN updated timestamp;
UPDATE bonita.documentdata set updated = created;
ALTER TABLE bonita.documentdata ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.documentdata ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.documenttype ADD COLUMN created timestamp;
UPDATE bonita.documenttype set created = version;
UPDATE bonita.documenttype set created = now() WHERE created is null;
ALTER TABLE bonita.documenttype ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.documenttype ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.documenttype ADD COLUMN updated timestamp;
UPDATE bonita.documenttype set updated = created;
ALTER TABLE bonita.documenttype ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.documenttype ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.eventaccessuseraccount ADD COLUMN created timestamp;
UPDATE bonita.eventaccessuseraccount set created = version;
UPDATE bonita.eventaccessuseraccount set created = now() WHERE created is null;
ALTER TABLE bonita.eventaccessuseraccount ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.eventaccessuseraccount ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.eventaccessuseraccount ADD COLUMN updated timestamp;
UPDATE bonita.eventaccessuseraccount set updated = created;
ALTER TABLE bonita.eventaccessuseraccount ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.eventaccessuseraccount ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.eventaccessuserrole ADD COLUMN created timestamp;
UPDATE bonita.eventaccessuserrole set created = version;
UPDATE bonita.eventaccessuserrole set created = now() WHERE created is null;
ALTER TABLE bonita.eventaccessuserrole ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.eventaccessuserrole ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.eventaccessuserrole ADD COLUMN updated timestamp;
UPDATE bonita.eventaccessuserrole set updated = created;
ALTER TABLE bonita.eventaccessuserrole ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.eventaccessuserrole ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.eventtype ADD COLUMN created timestamp;
UPDATE bonita.eventtype set created = version;
UPDATE bonita.eventtype set created = now() WHERE created is null;
ALTER TABLE bonita.eventtype ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.eventtype ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.eventtype ADD COLUMN updated timestamp;
UPDATE bonita.eventtype set updated = created;
ALTER TABLE bonita.eventtype ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.eventtype ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.eventtypestate ADD COLUMN created timestamp;
UPDATE bonita.eventtypestate set created = version;
UPDATE bonita.eventtypestate set created = now() WHERE created is null;
ALTER TABLE bonita.eventtypestate ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.eventtypestate ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.eventtypestate ADD COLUMN updated timestamp;
UPDATE bonita.eventtypestate set updated = created;
ALTER TABLE bonita.eventtypestate ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.eventtypestate ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.querytype ADD COLUMN created timestamp;
UPDATE bonita.querytype set created = version;
UPDATE bonita.querytype set created = now() WHERE created is null;
ALTER TABLE bonita.querytype ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.querytype ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.querytype ADD COLUMN updated timestamp;
UPDATE bonita.querytype set updated = created;
ALTER TABLE bonita.querytype ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.querytype ALTER COLUMN updated SET NOT NULL;

-- ADDED 22-JUN-2016.
alter table userrole add column accessreporting boolean not null default false;
update userrole set accessreporting = true where name='reviewer';


CREATE TABLE reportformattrialuserrolexref
(
  id bigint NOT NULL, 
  reportformattrial_id bigint NOT NULL references reportformattrialxref(id),
  userrole_id bigint NOT NULL references userrole(id),
  CONSTRAINT reportformattrialuserrolexref_pkey PRIMARY KEY (id),
  CONSTRAINT reportformattrialuserrolexref_key UNIQUE (reportformattrial_id, userrole_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE reportformattrialuserrolexref
  OWNER TO agmednet;
  
--initially, reviewer has access to all reportformattrials
CREATE OR REPLACE FUNCTION addReportingAccess() RETURNS boolean AS $$
	DECLARE
		myrec RECORD;
	BEGIN
		FOR myrec IN SELECT * from reportformattrialxref LOOP
			INSERT INTO reportformattrialuserrolexref (id,reportformattrial_id,userrole_id) VALUES(nextval('public.hibernate_sequence'),myrec.id,(select id from userrole where name = 'reviewer' limit 1));
		END LOOP;
		RETURN true;
	END;
$$ LANGUAGE plpgsql;

SELECT * from addReportingAccess();
drop function addReportingAccess();

ALTER TABLE reportformattrialxref ADD CONSTRAINT report_fkey FOREIGN KEY (report_id) REFERENCES public.report;
ALTER TABLE reportformattrialxref ADD CONSTRAINT trial_fkey FOREIGN KEY (trial_id) REFERENCES trial;
ALTER TABLE reportformattrialxref ADD CONSTRAINT reportformat_fkey FOREIGN KEY (reportformat_id) REFERENCES reportformat;


-- ADDED 24-JUN-2016
ALTER TABLE bonita.trialmetadata DROP CONSTRAINT "trialmetadata_sortorder_key";
CREATE UNIQUE INDEX "trialmetadata_unique_sortorder_trial" ON bonita.trialmetadata(trial_id,sortorder);


---------------------------
CREATE TABLE usersettingtype (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
	name text NOT NULL
);
CREATE UNIQUE INDEX "usersettingtype_unique_name" ON usersettingtype(name);


CREATE TABLE usersetting (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
	useraccount_id BIGINT NOT NULL REFERENCES useraccount(id),
	usersettingtype_id BIGINT NOT NULL REFERENCES usersettingtype(id),
    value text
);
CREATE UNIQUE INDEX "usersetting_unique_user_type" ON usersetting(useraccount_id,usersettingtype_id);

ALTER TABLE usersetting OWNER TO agmednet;
ALTER TABLE usersettingtype OWNER TO agmednet;

-- 06-JUL-2016 : fix for misconfigured trials JUDI-3288
UPDATE bonita.trialxml SET xml = '<actionEmails>' || xml || '</actionEmails>' WHERE xmltype_id = (select id from bonita.xmltype WHERE name = 'emails') AND xml NOT ILIKE '%</actionEmails>%';

-- 11-JUL-2016 
ALTER TABLE bonita.trialxml ADD COLUMN created timestamp;
UPDATE bonita.trialxml set created = version;
UPDATE bonita.trialxml set created = now() WHERE created is null;
ALTER TABLE bonita.trialxml ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.trialxml ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.trialxml ADD COLUMN updated timestamp;
UPDATE bonita.trialxml set updated = created;
ALTER TABLE bonita.trialxml ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.trialxml ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.xmltype ADD COLUMN created timestamp;
UPDATE bonita.xmltype set created = version;
UPDATE bonita.xmltype set created = now() WHERE created is null;
ALTER TABLE bonita.xmltype ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.xmltype ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.xmltype ADD COLUMN updated timestamp;
UPDATE bonita.xmltype set updated = created;
ALTER TABLE bonita.xmltype ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.xmltype ALTER COLUMN updated SET NOT NULL;


-- 18-JUL-2016 : added userroletrialmetadata table
CREATE TABLE bonita.userroletrialmetadata
(
  id bigint NOT NULL,
  userrole_id bigint,
  trialmetadata_id bigint,
  view boolean,
  edit boolean,
  CONSTRAINT userroletrialmetadata_pkey PRIMARY KEY (id),
  CONSTRAINT userroletrialmetadata_trialmetadata_fkey FOREIGN KEY (trialmetadata_id)
      REFERENCES bonita.trialmetadata (id),
  CONSTRAINT userroletrialmetadata_userrole_fkey FOREIGN KEY (userrole_id)
      REFERENCES userrole (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bonita.userroletrialmetadata
  OWNER TO agmednet;

ALTER TABLE bonita.userroletrialmetadata ADD CONSTRAINT userroletrialmetadata_one_is_true CHECK (view = true OR edit = true);

ALTER TABLE bonita.userroletrialmetadata ADD COLUMN created timestamp;
UPDATE bonita.userroletrialmetadata set created = now() WHERE created is null;
ALTER TABLE bonita.userroletrialmetadata ALTER COLUMN created SET DEFAULT now();
ALTER TABLE bonita.userroletrialmetadata ALTER COLUMN created SET NOT NULL;
ALTER TABLE bonita.userroletrialmetadata ADD COLUMN updated timestamp;
UPDATE bonita.userroletrialmetadata set updated = created;
ALTER TABLE bonita.userroletrialmetadata ALTER COLUMN updated SET DEFAULT now();
ALTER TABLE bonita.userroletrialmetadata ALTER COLUMN updated SET NOT NULL;

ALTER TABLE bonita.userroletrialmetadata ADD COLUMN version timestamp;
UPDATE bonita.userroletrialmetadata set version = created;
ALTER TABLE bonita.userroletrialmetadata ALTER COLUMN version SET DEFAULT now();
ALTER TABLE bonita.userroletrialmetadata ALTER COLUMN version SET NOT NULL;

ALTER TABLE bonita.userroletrialmetadata ADD CONSTRAINT userroletrialmetadata_userrole_trialmetadata UNIQUE(userrole_id,trialmetadata_id);

ALTER TABLE public.historyevent ADD COLUMN useractiontime TIMESTAMP;


-- 18-JUL-2016 : added report format jrxml table
CREATE TABLE reportformatjrxml
(
  id bigint NOT NULL,
  report_id bigint NOT NULL,
  reportformat_id bigint NOT NULL,
  compiledreport bytea,
  version timestamp without time zone,
  created timestamp without time zone NOT NULL,
  updated timestamp without time zone NOT NULL,
  CONSTRAINT reportformatjrxml_pkey PRIMARY KEY (id),
  CONSTRAINT reportformatjrxml_report_fkey FOREIGN KEY (report_id)
      REFERENCES public.report (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT reportformatjrxml_reportformat_fkey FOREIGN KEY (reportformat_id)
      REFERENCES reportformat (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT reportformatjrxml_ukey UNIQUE (report_id, reportformat_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE reportformatjrxml
  OWNER TO agmednet;


INSERT INTO reportformatjrxml (id, report_id,compiledreport, reportformat_id,version,created,updated)
SELECT  nextval('public.hibernate_sequence'), r.id, r.compiled, f.id, now(),now(),now()
FROM public.report r, reportformat f;

ALTER TABLE public.report DROP COLUMN compiled;

-- ADDED 20-JUL-2016
insert into usersettingtype (id, version, created, updated, name) values (nextval('public.hibernate_sequence'), now(), now(), now(), 'eventDetails');

--JUDI-3419
CREATE TABLE trialsetting (
    id bigint NOT NULL PRIMARY KEY,
    version timestamp without time zone,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    trial_id BIGINT NOT NULL REFERENCES trial(id),
    name text NOT NULL,
	type text NOT NULL,
	numberval bigint,
	dateval date,
	stringval text,
	boolval boolean
	CHECK ( (type = 'number' and numberval IS NOT NULL and dateval IS NULL and stringval IS NULL and boolval IS NULL)
			OR (type = 'date' and numberval IS NULL and dateval IS NOT NULL and stringval IS NULL and boolval IS NULL)
			OR (type = 'string' and numberval IS NULL and dateval IS NULL and stringval IS NOT NULL and boolval IS NULL)
			OR (type = 'boolean' and numberval IS NULL and dateval IS NULL and stringval IS NULL and boolval IS NOT NULL) )
);
CREATE UNIQUE INDEX "trialsetting_unique_name_per_trial" ON trialsetting(trial_id,name);

GRANT ALL ON TABLE trialsetting TO "spring";

-- ACLs
CREATE USER "servicemix-securitymanager" WITH PASSWORD 'servicemess';

GRANT USAGE ON SCHEMA bonita TO "servicemix-securitymanager";

GRANT USAGE ON SCHEMA bonita TO readonly;

SET search_path = bonita, pg_catalog;

--
-- Name: actor; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE actor FROM PUBLIC;
REVOKE ALL ON TABLE actor FROM bonita;
GRANT ALL ON TABLE actor TO bonita;
GRANT ALL ON TABLE actor TO agmednet;
GRANT SELECT ON TABLE actor TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE actor TO "servicemix-securitymanager";
GRANT SELECT ON TABLE actor TO readonly;


--
-- Name: actormember; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE actormember FROM PUBLIC;
REVOKE ALL ON TABLE actormember FROM bonita;
GRANT ALL ON TABLE actormember TO bonita;
GRANT ALL ON TABLE actormember TO agmednet;
GRANT SELECT ON TABLE actormember TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE actormember TO "servicemix-securitymanager";
GRANT SELECT ON TABLE actormember TO readonly;


--
-- Name: arch_connector_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_connector_instance FROM PUBLIC;
REVOKE ALL ON TABLE arch_connector_instance FROM bonita;
GRANT ALL ON TABLE arch_connector_instance TO bonita;
GRANT ALL ON TABLE arch_connector_instance TO agmednet;
GRANT SELECT ON TABLE arch_connector_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_connector_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_connector_instance TO readonly;


--
-- Name: arch_data_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_data_instance FROM PUBLIC;
REVOKE ALL ON TABLE arch_data_instance FROM bonita;
GRANT ALL ON TABLE arch_data_instance TO bonita;
GRANT ALL ON TABLE arch_data_instance TO agmednet;
GRANT SELECT ON TABLE arch_data_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_data_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_data_instance TO readonly;


--
-- Name: arch_data_mapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_data_mapping FROM PUBLIC;
REVOKE ALL ON TABLE arch_data_mapping FROM bonita;
GRANT ALL ON TABLE arch_data_mapping TO bonita;
GRANT ALL ON TABLE arch_data_mapping TO agmednet;
GRANT SELECT ON TABLE arch_data_mapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_data_mapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_data_mapping TO readonly;


--
-- Name: arch_document_mapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_document_mapping FROM PUBLIC;
REVOKE ALL ON TABLE arch_document_mapping FROM bonita;
GRANT ALL ON TABLE arch_document_mapping TO bonita;
GRANT ALL ON TABLE arch_document_mapping TO agmednet;
GRANT SELECT ON TABLE arch_document_mapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_document_mapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_document_mapping TO readonly;


--
-- Name: arch_flownode_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_flownode_instance FROM PUBLIC;
REVOKE ALL ON TABLE arch_flownode_instance FROM bonita;
GRANT ALL ON TABLE arch_flownode_instance TO bonita;
GRANT ALL ON TABLE arch_flownode_instance TO agmednet;
GRANT SELECT ON TABLE arch_flownode_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_flownode_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_flownode_instance TO readonly;


--
-- Name: arch_process_comment; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_process_comment FROM PUBLIC;
REVOKE ALL ON TABLE arch_process_comment FROM bonita;
GRANT ALL ON TABLE arch_process_comment TO bonita;
GRANT ALL ON TABLE arch_process_comment TO agmednet;
GRANT SELECT ON TABLE arch_process_comment TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_process_comment TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_process_comment TO readonly;


--
-- Name: arch_process_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_process_instance FROM PUBLIC;
REVOKE ALL ON TABLE arch_process_instance FROM bonita;
GRANT ALL ON TABLE arch_process_instance TO bonita;
GRANT ALL ON TABLE arch_process_instance TO agmednet;
GRANT SELECT ON TABLE arch_process_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_process_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_process_instance TO readonly;


--
-- Name: arch_transition_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE arch_transition_instance FROM PUBLIC;
REVOKE ALL ON TABLE arch_transition_instance FROM bonita;
GRANT ALL ON TABLE arch_transition_instance TO bonita;
GRANT ALL ON TABLE arch_transition_instance TO agmednet;
GRANT SELECT ON TABLE arch_transition_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE arch_transition_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE arch_transition_instance TO readonly;


--
-- Name: baselinedossier; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE baselinedossier FROM PUBLIC;
REVOKE ALL ON TABLE baselinedossier FROM bonita;
GRANT ALL ON TABLE baselinedossier TO bonita;
GRANT ALL ON TABLE baselinedossier TO agmednet;
GRANT SELECT ON TABLE baselinedossier TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE baselinedossier TO "servicemix-securitymanager";
GRANT SELECT ON TABLE baselinedossier TO readonly;


--
-- Name: baselinedossierdocumentdataxref; Type: ACL; Schema: bonita; Owner: servicemix-adjudication
--

REVOKE ALL ON TABLE baselinedossierdocumentdataxref FROM PUBLIC;
REVOKE ALL ON TABLE baselinedossierdocumentdataxref FROM "servicemix-adjudication";
GRANT ALL ON TABLE baselinedossierdocumentdataxref TO "servicemix-adjudication";
GRANT SELECT ON TABLE baselinedossierdocumentdataxref TO readonlyuser;


--
-- Name: baselinedossierqueryxref; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE baselinedossierqueryxref FROM PUBLIC;
REVOKE ALL ON TABLE baselinedossierqueryxref FROM bonita;
GRANT ALL ON TABLE baselinedossierqueryxref TO bonita;
GRANT ALL ON TABLE baselinedossierqueryxref TO agmednet;
GRANT SELECT ON TABLE baselinedossierqueryxref TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE baselinedossierqueryxref TO "servicemix-securitymanager";
GRANT SELECT ON TABLE baselinedossierqueryxref TO readonly;


--
-- Name: breakpoint; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE breakpoint FROM PUBLIC;
REVOKE ALL ON TABLE breakpoint FROM bonita;
GRANT ALL ON TABLE breakpoint TO bonita;
GRANT ALL ON TABLE breakpoint TO agmednet;
GRANT SELECT ON TABLE breakpoint TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE breakpoint TO "servicemix-securitymanager";
GRANT SELECT ON TABLE breakpoint TO readonly;


--
-- Name: category; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE category FROM PUBLIC;
REVOKE ALL ON TABLE category FROM bonita;
GRANT ALL ON TABLE category TO bonita;
GRANT ALL ON TABLE category TO agmednet;
GRANT SELECT ON TABLE category TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE category TO "servicemix-securitymanager";
GRANT SELECT ON TABLE category TO readonly;


--
-- Name: command; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE command FROM PUBLIC;
REVOKE ALL ON TABLE command FROM bonita;
GRANT ALL ON TABLE command TO bonita;
GRANT ALL ON TABLE command TO agmednet;
GRANT SELECT ON TABLE command TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE command TO "servicemix-securitymanager";
GRANT SELECT ON TABLE command TO readonly;


--
-- Name: connector_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE connector_instance FROM PUBLIC;
REVOKE ALL ON TABLE connector_instance FROM bonita;
GRANT ALL ON TABLE connector_instance TO bonita;
GRANT ALL ON TABLE connector_instance TO agmednet;
GRANT SELECT ON TABLE connector_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE connector_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE connector_instance TO readonly;


--
-- Name: custom_usr_inf_def; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE custom_usr_inf_def FROM PUBLIC;
REVOKE ALL ON TABLE custom_usr_inf_def FROM bonita;
GRANT ALL ON TABLE custom_usr_inf_def TO bonita;
GRANT ALL ON TABLE custom_usr_inf_def TO agmednet;
GRANT SELECT ON TABLE custom_usr_inf_def TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE custom_usr_inf_def TO "servicemix-securitymanager";
GRANT SELECT ON TABLE custom_usr_inf_def TO readonly;


--
-- Name: custom_usr_inf_val; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE custom_usr_inf_val FROM PUBLIC;
REVOKE ALL ON TABLE custom_usr_inf_val FROM bonita;
GRANT ALL ON TABLE custom_usr_inf_val TO bonita;
GRANT ALL ON TABLE custom_usr_inf_val TO agmednet;
GRANT SELECT ON TABLE custom_usr_inf_val TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE custom_usr_inf_val TO "servicemix-securitymanager";
GRANT SELECT ON TABLE custom_usr_inf_val TO readonly;


--
-- Name: data_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE data_instance FROM PUBLIC;
REVOKE ALL ON TABLE data_instance FROM bonita;
GRANT ALL ON TABLE data_instance TO bonita;
GRANT ALL ON TABLE data_instance TO agmednet;
GRANT SELECT ON TABLE data_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE data_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE data_instance TO readonly;


--
-- Name: data_mapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE data_mapping FROM PUBLIC;
REVOKE ALL ON TABLE data_mapping FROM bonita;
GRANT ALL ON TABLE data_mapping TO bonita;
GRANT ALL ON TABLE data_mapping TO agmednet;
GRANT SELECT ON TABLE data_mapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE data_mapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE data_mapping TO readonly;


--
-- Name: dependency; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE dependency FROM PUBLIC;
REVOKE ALL ON TABLE dependency FROM bonita;
GRANT ALL ON TABLE dependency TO bonita;
GRANT ALL ON TABLE dependency TO agmednet;
GRANT SELECT ON TABLE dependency TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE dependency TO "servicemix-securitymanager";
GRANT SELECT ON TABLE dependency TO readonly;


--
-- Name: dependencymapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE dependencymapping FROM PUBLIC;
REVOKE ALL ON TABLE dependencymapping FROM bonita;
GRANT ALL ON TABLE dependencymapping TO bonita;
GRANT ALL ON TABLE dependencymapping TO agmednet;
GRANT SELECT ON TABLE dependencymapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE dependencymapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE dependencymapping TO readonly;


--
-- Name: document; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE document FROM PUBLIC;
REVOKE ALL ON TABLE document FROM bonita;
GRANT ALL ON TABLE document TO bonita;
GRANT ALL ON TABLE document TO agmednet;
GRANT SELECT ON TABLE document TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE document TO "servicemix-securitymanager";
GRANT SELECT ON TABLE document TO readonly;


--
-- Name: document_content; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE document_content FROM PUBLIC;
REVOKE ALL ON TABLE document_content FROM bonita;
GRANT ALL ON TABLE document_content TO bonita;
GRANT ALL ON TABLE document_content TO agmednet;
GRANT SELECT ON TABLE document_content TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE document_content TO "servicemix-securitymanager";
GRANT SELECT ON TABLE document_content TO readonly;


--
-- Name: document_mapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE document_mapping FROM PUBLIC;
REVOKE ALL ON TABLE document_mapping FROM bonita;
GRANT ALL ON TABLE document_mapping TO bonita;
GRANT ALL ON TABLE document_mapping TO agmednet;
GRANT SELECT ON TABLE document_mapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE document_mapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE document_mapping TO readonly;


--
-- Name: documentdata; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE documentdata FROM PUBLIC;
REVOKE ALL ON TABLE documentdata FROM bonita;
GRANT ALL ON TABLE documentdata TO bonita;
GRANT ALL ON TABLE documentdata TO agmednet;
GRANT SELECT ON TABLE documentdata TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE documentdata TO "servicemix-securitymanager";
GRANT SELECT ON TABLE documentdata TO readonly;


--
-- Name: documenttype; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE documenttype FROM PUBLIC;
REVOKE ALL ON TABLE documenttype FROM bonita;
GRANT ALL ON TABLE documenttype TO bonita;
GRANT ALL ON TABLE documenttype TO agmednet;
GRANT SELECT ON TABLE documenttype TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE documenttype TO "servicemix-securitymanager";
GRANT SELECT ON TABLE documenttype TO readonly;


--
-- Name: dossierview; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE dossierview FROM PUBLIC;
REVOKE ALL ON TABLE dossierview FROM bonita;
GRANT ALL ON TABLE dossierview TO bonita;
GRANT ALL ON TABLE dossierview TO agmednet;
GRANT SELECT ON TABLE dossierview TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE dossierview TO "servicemix-securitymanager";
GRANT SELECT ON TABLE dossierview TO readonly;


--
-- Name: ecrf; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE ecrf FROM PUBLIC;
REVOKE ALL ON TABLE ecrf FROM bonita;
GRANT ALL ON TABLE ecrf TO bonita;
GRANT ALL ON TABLE ecrf TO agmednet;
GRANT SELECT ON TABLE ecrf TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ecrf TO "servicemix-securitymanager";
GRANT SELECT ON TABLE ecrf TO readonly;


--
-- Name: ecrfresult; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE ecrfresult FROM PUBLIC;
REVOKE ALL ON TABLE ecrfresult FROM bonita;
GRANT ALL ON TABLE ecrfresult TO bonita;
GRANT ALL ON TABLE ecrfresult TO agmednet;
GRANT SELECT ON TABLE ecrfresult TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ecrfresult TO "servicemix-securitymanager";
GRANT SELECT ON TABLE ecrfresult TO readonly;


--
-- Name: event_trigger_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE event_trigger_instance FROM PUBLIC;
REVOKE ALL ON TABLE event_trigger_instance FROM bonita;
GRANT ALL ON TABLE event_trigger_instance TO bonita;
GRANT ALL ON TABLE event_trigger_instance TO agmednet;
GRANT SELECT ON TABLE event_trigger_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE event_trigger_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE event_trigger_instance TO readonly;


--
-- Name: eventaccessuseraccount; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE eventaccessuseraccount FROM PUBLIC;
REVOKE ALL ON TABLE eventaccessuseraccount FROM bonita;
GRANT ALL ON TABLE eventaccessuseraccount TO bonita;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE eventaccessuseraccount TO "servicemix-securitymanager";
GRANT SELECT ON TABLE eventaccessuseraccount TO readonlyuser;
GRANT SELECT ON TABLE eventaccessuseraccount TO readonly;


--
-- Name: eventaccessuserrole; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE eventaccessuserrole FROM PUBLIC;
REVOKE ALL ON TABLE eventaccessuserrole FROM bonita;
GRANT ALL ON TABLE eventaccessuserrole TO bonita;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE eventaccessuserrole TO "servicemix-securitymanager";
GRANT SELECT ON TABLE eventaccessuserrole TO readonlyuser;
GRANT SELECT ON TABLE eventaccessuserrole TO readonly;


--
-- Name: eventinstance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE eventinstance FROM PUBLIC;
REVOKE ALL ON TABLE eventinstance FROM bonita;
GRANT ALL ON TABLE eventinstance TO bonita;
GRANT ALL ON TABLE eventinstance TO agmednet;
GRANT SELECT ON TABLE eventinstance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE eventinstance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE eventinstance TO readonly;


--
-- Name: eventmetadata; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE eventmetadata FROM PUBLIC;
REVOKE ALL ON TABLE eventmetadata FROM bonita;
GRANT ALL ON TABLE eventmetadata TO bonita;
GRANT SELECT ON TABLE eventmetadata TO readonlyuser;
GRANT SELECT ON TABLE eventmetadata TO readonly;


--
-- Name: eventtype; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE eventtype FROM PUBLIC;
REVOKE ALL ON TABLE eventtype FROM bonita;
GRANT ALL ON TABLE eventtype TO bonita;
GRANT ALL ON TABLE eventtype TO agmednet;
GRANT SELECT ON TABLE eventtype TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE eventtype TO "servicemix-securitymanager";
GRANT SELECT ON TABLE eventtype TO readonly;


--
-- Name: eventtypestate; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE eventtypestate FROM PUBLIC;
REVOKE ALL ON TABLE eventtypestate FROM bonita;
GRANT ALL ON TABLE eventtypestate TO bonita;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE eventtypestate TO "servicemix-securitymanager";
GRANT SELECT ON TABLE eventtypestate TO readonlyuser;
GRANT SELECT ON TABLE eventtypestate TO readonly;


--
-- Name: external_identity_mapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE external_identity_mapping FROM PUBLIC;
REVOKE ALL ON TABLE external_identity_mapping FROM bonita;
GRANT ALL ON TABLE external_identity_mapping TO bonita;
GRANT ALL ON TABLE external_identity_mapping TO agmednet;
GRANT SELECT ON TABLE external_identity_mapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE external_identity_mapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE external_identity_mapping TO readonly;


--
-- Name: finaloutcome; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE finaloutcome FROM PUBLIC;
REVOKE ALL ON TABLE finaloutcome FROM bonita;
GRANT ALL ON TABLE finaloutcome TO bonita;
GRANT SELECT ON TABLE finaloutcome TO readonlyuser;
GRANT SELECT ON TABLE finaloutcome TO readonly;


--
-- Name: flownode_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE flownode_instance FROM PUBLIC;
REVOKE ALL ON TABLE flownode_instance FROM bonita;
GRANT ALL ON TABLE flownode_instance TO bonita;
GRANT ALL ON TABLE flownode_instance TO agmednet;
GRANT SELECT ON TABLE flownode_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE flownode_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE flownode_instance TO readonly;


--
-- Name: form; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE form FROM PUBLIC;
REVOKE ALL ON TABLE form FROM bonita;
GRANT ALL ON TABLE form TO bonita;
GRANT SELECT ON TABLE form TO readonlyuser;
GRANT SELECT ON TABLE form TO readonly;


--
-- Name: formresult; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE formresult FROM PUBLIC;
REVOKE ALL ON TABLE formresult FROM bonita;
GRANT ALL ON TABLE formresult TO bonita;
GRANT SELECT ON TABLE formresult TO readonlyuser;
GRANT SELECT ON TABLE formresult TO readonly;


--
-- Name: group_; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE group_ FROM PUBLIC;
REVOKE ALL ON TABLE group_ FROM bonita;
GRANT ALL ON TABLE group_ TO bonita;
GRANT ALL ON TABLE group_ TO agmednet;
GRANT SELECT ON TABLE group_ TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE group_ TO "servicemix-securitymanager";
GRANT SELECT ON TABLE group_ TO readonly;


--
-- Name: hidden_activity; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE hidden_activity FROM PUBLIC;
REVOKE ALL ON TABLE hidden_activity FROM bonita;
GRANT ALL ON TABLE hidden_activity TO bonita;
GRANT ALL ON TABLE hidden_activity TO agmednet;
GRANT SELECT ON TABLE hidden_activity TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE hidden_activity TO "servicemix-securitymanager";
GRANT SELECT ON TABLE hidden_activity TO readonly;


--
-- Name: job_desc; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE job_desc FROM PUBLIC;
REVOKE ALL ON TABLE job_desc FROM bonita;
GRANT ALL ON TABLE job_desc TO bonita;
GRANT ALL ON TABLE job_desc TO agmednet;
GRANT SELECT ON TABLE job_desc TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE job_desc TO "servicemix-securitymanager";
GRANT SELECT ON TABLE job_desc TO readonly;


--
-- Name: job_log; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE job_log FROM PUBLIC;
REVOKE ALL ON TABLE job_log FROM bonita;
GRANT ALL ON TABLE job_log TO bonita;
GRANT ALL ON TABLE job_log TO agmednet;
GRANT SELECT ON TABLE job_log TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE job_log TO "servicemix-securitymanager";
GRANT SELECT ON TABLE job_log TO readonly;


--
-- Name: job_param; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE job_param FROM PUBLIC;
REVOKE ALL ON TABLE job_param FROM bonita;
GRANT ALL ON TABLE job_param TO bonita;
GRANT ALL ON TABLE job_param TO agmednet;
GRANT SELECT ON TABLE job_param TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE job_param TO "servicemix-securitymanager";
GRANT SELECT ON TABLE job_param TO readonly;


--
-- Name: message_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE message_instance FROM PUBLIC;
REVOKE ALL ON TABLE message_instance FROM bonita;
GRANT ALL ON TABLE message_instance TO bonita;
GRANT ALL ON TABLE message_instance TO agmednet;
GRANT SELECT ON TABLE message_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE message_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE message_instance TO readonly;


--
-- Name: missingdocument; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE missingdocument FROM PUBLIC;
REVOKE ALL ON TABLE missingdocument FROM bonita;
GRANT ALL ON TABLE missingdocument TO bonita;
GRANT ALL ON TABLE missingdocument TO agmednet;
GRANT SELECT ON TABLE missingdocument TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE missingdocument TO "servicemix-securitymanager";
GRANT SELECT ON TABLE missingdocument TO readonly;


--
-- Name: page; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE page FROM PUBLIC;
REVOKE ALL ON TABLE page FROM bonita;
GRANT ALL ON TABLE page TO bonita;
GRANT ALL ON TABLE page TO agmednet;
GRANT SELECT ON TABLE page TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE page TO "servicemix-securitymanager";
GRANT SELECT ON TABLE page TO readonly;


--
-- Name: pdependency; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE pdependency FROM PUBLIC;
REVOKE ALL ON TABLE pdependency FROM bonita;
GRANT ALL ON TABLE pdependency TO bonita;
GRANT ALL ON TABLE pdependency TO agmednet;
GRANT SELECT ON TABLE pdependency TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pdependency TO "servicemix-securitymanager";
GRANT SELECT ON TABLE pdependency TO readonly;


--
-- Name: pdependencymapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE pdependencymapping FROM PUBLIC;
REVOKE ALL ON TABLE pdependencymapping FROM bonita;
GRANT ALL ON TABLE pdependencymapping TO bonita;
GRANT ALL ON TABLE pdependencymapping TO agmednet;
GRANT SELECT ON TABLE pdependencymapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pdependencymapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE pdependencymapping TO readonly;


--
-- Name: pending_mapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE pending_mapping FROM PUBLIC;
REVOKE ALL ON TABLE pending_mapping FROM bonita;
GRANT ALL ON TABLE pending_mapping TO bonita;
GRANT ALL ON TABLE pending_mapping TO agmednet;
GRANT SELECT ON TABLE pending_mapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pending_mapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE pending_mapping TO readonly;


--
-- Name: platform; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE platform FROM PUBLIC;
REVOKE ALL ON TABLE platform FROM bonita;
GRANT ALL ON TABLE platform TO bonita;
GRANT ALL ON TABLE platform TO agmednet;
GRANT SELECT ON TABLE platform TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE platform TO "servicemix-securitymanager";
GRANT SELECT ON TABLE platform TO readonly;


--
-- Name: platformcommand; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE platformcommand FROM PUBLIC;
REVOKE ALL ON TABLE platformcommand FROM bonita;
GRANT ALL ON TABLE platformcommand TO bonita;
GRANT ALL ON TABLE platformcommand TO agmednet;
GRANT SELECT ON TABLE platformcommand TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE platformcommand TO "servicemix-securitymanager";
GRANT SELECT ON TABLE platformcommand TO readonly;


--
-- Name: process_comment; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE process_comment FROM PUBLIC;
REVOKE ALL ON TABLE process_comment FROM bonita;
GRANT ALL ON TABLE process_comment TO bonita;
GRANT ALL ON TABLE process_comment TO agmednet;
GRANT SELECT ON TABLE process_comment TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE process_comment TO "servicemix-securitymanager";
GRANT SELECT ON TABLE process_comment TO readonly;


--
-- Name: process_definition; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE process_definition FROM PUBLIC;
REVOKE ALL ON TABLE process_definition FROM bonita;
GRANT ALL ON TABLE process_definition TO bonita;
GRANT ALL ON TABLE process_definition TO agmednet;
GRANT SELECT ON TABLE process_definition TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE process_definition TO "servicemix-securitymanager";
GRANT SELECT ON TABLE process_definition TO readonly;


--
-- Name: process_instance; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE process_instance FROM PUBLIC;
REVOKE ALL ON TABLE process_instance FROM bonita;
GRANT ALL ON TABLE process_instance TO bonita;
GRANT ALL ON TABLE process_instance TO agmednet;
GRANT SELECT ON TABLE process_instance TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE process_instance TO "servicemix-securitymanager";
GRANT SELECT ON TABLE process_instance TO readonly;


--
-- Name: processcategorymapping; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE processcategorymapping FROM PUBLIC;
REVOKE ALL ON TABLE processcategorymapping FROM bonita;
GRANT ALL ON TABLE processcategorymapping TO bonita;
GRANT ALL ON TABLE processcategorymapping TO agmednet;
GRANT SELECT ON TABLE processcategorymapping TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE processcategorymapping TO "servicemix-securitymanager";
GRANT SELECT ON TABLE processcategorymapping TO readonly;


--
-- Name: processsupervisor; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE processsupervisor FROM PUBLIC;
REVOKE ALL ON TABLE processsupervisor FROM bonita;
GRANT ALL ON TABLE processsupervisor TO bonita;
GRANT ALL ON TABLE processsupervisor TO agmednet;
GRANT SELECT ON TABLE processsupervisor TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE processsupervisor TO "servicemix-securitymanager";
GRANT SELECT ON TABLE processsupervisor TO readonly;


--
-- Name: profile; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE profile FROM PUBLIC;
REVOKE ALL ON TABLE profile FROM bonita;
GRANT ALL ON TABLE profile TO bonita;
GRANT ALL ON TABLE profile TO agmednet;
GRANT SELECT ON TABLE profile TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE profile TO "servicemix-securitymanager";
GRANT SELECT ON TABLE profile TO readonly;


--
-- Name: profileentry; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE profileentry FROM PUBLIC;
REVOKE ALL ON TABLE profileentry FROM bonita;
GRANT ALL ON TABLE profileentry TO bonita;
GRANT ALL ON TABLE profileentry TO agmednet;
GRANT SELECT ON TABLE profileentry TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE profileentry TO "servicemix-securitymanager";
GRANT SELECT ON TABLE profileentry TO readonly;


--
-- Name: profilemember; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE profilemember FROM PUBLIC;
REVOKE ALL ON TABLE profilemember FROM bonita;
GRANT ALL ON TABLE profilemember TO bonita;
GRANT ALL ON TABLE profilemember TO agmednet;
GRANT SELECT ON TABLE profilemember TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE profilemember TO "servicemix-securitymanager";
GRANT SELECT ON TABLE profilemember TO readonly;


--
-- Name: qrtz_blob_triggers; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_blob_triggers FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_blob_triggers FROM bonita;
GRANT ALL ON TABLE qrtz_blob_triggers TO bonita;
GRANT ALL ON TABLE qrtz_blob_triggers TO agmednet;
GRANT SELECT ON TABLE qrtz_blob_triggers TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_blob_triggers TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_blob_triggers TO readonly;


--
-- Name: qrtz_calendars; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_calendars FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_calendars FROM bonita;
GRANT ALL ON TABLE qrtz_calendars TO bonita;
GRANT ALL ON TABLE qrtz_calendars TO agmednet;
GRANT SELECT ON TABLE qrtz_calendars TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_calendars TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_calendars TO readonly;


--
-- Name: qrtz_cron_triggers; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_cron_triggers FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_cron_triggers FROM bonita;
GRANT ALL ON TABLE qrtz_cron_triggers TO bonita;
GRANT ALL ON TABLE qrtz_cron_triggers TO agmednet;
GRANT SELECT ON TABLE qrtz_cron_triggers TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_cron_triggers TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_cron_triggers TO readonly;


--
-- Name: qrtz_fired_triggers; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_fired_triggers FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_fired_triggers FROM bonita;
GRANT ALL ON TABLE qrtz_fired_triggers TO bonita;
GRANT ALL ON TABLE qrtz_fired_triggers TO agmednet;
GRANT SELECT ON TABLE qrtz_fired_triggers TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_fired_triggers TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_fired_triggers TO readonly;


--
-- Name: qrtz_job_details; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_job_details FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_job_details FROM bonita;
GRANT ALL ON TABLE qrtz_job_details TO bonita;
GRANT ALL ON TABLE qrtz_job_details TO agmednet;
GRANT SELECT ON TABLE qrtz_job_details TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_job_details TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_job_details TO readonly;


--
-- Name: qrtz_locks; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_locks FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_locks FROM bonita;
GRANT ALL ON TABLE qrtz_locks TO bonita;
GRANT ALL ON TABLE qrtz_locks TO agmednet;
GRANT SELECT ON TABLE qrtz_locks TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_locks TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_locks TO readonly;


--
-- Name: qrtz_paused_trigger_grps; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_paused_trigger_grps FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_paused_trigger_grps FROM bonita;
GRANT ALL ON TABLE qrtz_paused_trigger_grps TO bonita;
GRANT ALL ON TABLE qrtz_paused_trigger_grps TO agmednet;
GRANT SELECT ON TABLE qrtz_paused_trigger_grps TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_paused_trigger_grps TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_paused_trigger_grps TO readonly;


--
-- Name: qrtz_scheduler_state; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_scheduler_state FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_scheduler_state FROM bonita;
GRANT ALL ON TABLE qrtz_scheduler_state TO bonita;
GRANT ALL ON TABLE qrtz_scheduler_state TO agmednet;
GRANT SELECT ON TABLE qrtz_scheduler_state TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_scheduler_state TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_scheduler_state TO readonly;


--
-- Name: qrtz_simple_triggers; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_simple_triggers FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_simple_triggers FROM bonita;
GRANT ALL ON TABLE qrtz_simple_triggers TO bonita;
GRANT ALL ON TABLE qrtz_simple_triggers TO agmednet;
GRANT SELECT ON TABLE qrtz_simple_triggers TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_simple_triggers TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_simple_triggers TO readonly;


--
-- Name: qrtz_simprop_triggers; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_simprop_triggers FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_simprop_triggers FROM bonita;
GRANT ALL ON TABLE qrtz_simprop_triggers TO bonita;
GRANT ALL ON TABLE qrtz_simprop_triggers TO agmednet;
GRANT SELECT ON TABLE qrtz_simprop_triggers TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_simprop_triggers TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_simprop_triggers TO readonly;


--
-- Name: qrtz_triggers; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE qrtz_triggers FROM PUBLIC;
REVOKE ALL ON TABLE qrtz_triggers FROM bonita;
GRANT ALL ON TABLE qrtz_triggers TO bonita;
GRANT ALL ON TABLE qrtz_triggers TO agmednet;
GRANT SELECT ON TABLE qrtz_triggers TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE qrtz_triggers TO "servicemix-securitymanager";
GRANT SELECT ON TABLE qrtz_triggers TO readonly;


--
-- Name: queriable_log; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE queriable_log FROM PUBLIC;
REVOKE ALL ON TABLE queriable_log FROM bonita;
GRANT ALL ON TABLE queriable_log TO bonita;
GRANT ALL ON TABLE queriable_log TO agmednet;
GRANT SELECT ON TABLE queriable_log TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE queriable_log TO "servicemix-securitymanager";
GRANT SELECT ON TABLE queriable_log TO readonly;


--
-- Name: queriablelog_p; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE queriablelog_p FROM PUBLIC;
REVOKE ALL ON TABLE queriablelog_p FROM bonita;
GRANT ALL ON TABLE queriablelog_p TO bonita;
GRANT ALL ON TABLE queriablelog_p TO agmednet;
GRANT SELECT ON TABLE queriablelog_p TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE queriablelog_p TO "servicemix-securitymanager";
GRANT SELECT ON TABLE queriablelog_p TO readonly;


--
-- Name: query; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE query FROM PUBLIC;
REVOKE ALL ON TABLE query FROM bonita;
GRANT ALL ON TABLE query TO bonita;
GRANT ALL ON TABLE query TO agmednet;
GRANT SELECT ON TABLE query TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE query TO "servicemix-securitymanager";
GRANT SELECT ON TABLE query TO readonly;


--
-- Name: querycomment; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE querycomment FROM PUBLIC;
REVOKE ALL ON TABLE querycomment FROM bonita;
GRANT ALL ON TABLE querycomment TO bonita;
GRANT ALL ON TABLE querycomment TO agmednet;
GRANT SELECT ON TABLE querycomment TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE querycomment TO "servicemix-securitymanager";
GRANT SELECT ON TABLE querycomment TO readonly;


--
-- Name: querytype; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE querytype FROM PUBLIC;
REVOKE ALL ON TABLE querytype FROM bonita;
GRANT ALL ON TABLE querytype TO bonita;
GRANT ALL ON TABLE querytype TO agmednet;
GRANT SELECT ON TABLE querytype TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE querytype TO "servicemix-securitymanager";
GRANT SELECT ON TABLE querytype TO readonly;


--
-- Name: ref_biz_data_inst; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE ref_biz_data_inst FROM PUBLIC;
REVOKE ALL ON TABLE ref_biz_data_inst FROM bonita;
GRANT ALL ON TABLE ref_biz_data_inst TO bonita;
GRANT ALL ON TABLE ref_biz_data_inst TO agmednet;
GRANT SELECT ON TABLE ref_biz_data_inst TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ref_biz_data_inst TO "servicemix-securitymanager";
GRANT SELECT ON TABLE ref_biz_data_inst TO readonly;


--
-- Name: report; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE report FROM PUBLIC;
REVOKE ALL ON TABLE report FROM bonita;
GRANT ALL ON TABLE report TO bonita;
GRANT ALL ON TABLE report TO agmednet;
GRANT SELECT ON TABLE report TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE report TO "servicemix-securitymanager";
GRANT SELECT ON TABLE report TO readonly;


--
-- Name: reviewerform; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE reviewerform FROM PUBLIC;
REVOKE ALL ON TABLE reviewerform FROM bonita;
GRANT ALL ON TABLE reviewerform TO bonita;
GRANT ALL ON TABLE reviewerform TO agmednet;
GRANT SELECT ON TABLE reviewerform TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE reviewerform TO "servicemix-securitymanager";
GRANT SELECT ON TABLE reviewerform TO readonly;


--
-- Name: reviewerformresult; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE reviewerformresult FROM PUBLIC;
REVOKE ALL ON TABLE reviewerformresult FROM bonita;
GRANT ALL ON TABLE reviewerformresult TO bonita;
GRANT ALL ON TABLE reviewerformresult TO agmednet;
GRANT SELECT ON TABLE reviewerformresult TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE reviewerformresult TO "servicemix-securitymanager";
GRANT SELECT ON TABLE reviewerformresult TO readonly;


--
-- Name: role; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE role FROM PUBLIC;
REVOKE ALL ON TABLE role FROM bonita;
GRANT ALL ON TABLE role TO bonita;
GRANT ALL ON TABLE role TO agmednet;
GRANT SELECT ON TABLE role TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE role TO "servicemix-securitymanager";
GRANT SELECT ON TABLE role TO readonly;


--
-- Name: sequence; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE sequence FROM PUBLIC;
REVOKE ALL ON TABLE sequence FROM bonita;
GRANT ALL ON TABLE sequence TO bonita;
GRANT ALL ON TABLE sequence TO agmednet;
GRANT SELECT ON TABLE sequence TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE sequence TO "servicemix-securitymanager";
GRANT SELECT ON TABLE sequence TO readonly;


--
-- Name: task; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE task FROM PUBLIC;
REVOKE ALL ON TABLE task FROM bonita;
GRANT ALL ON TABLE task TO bonita;
GRANT ALL ON TABLE task TO agmednet;
GRANT SELECT ON TABLE task TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE task TO "servicemix-securitymanager";
GRANT SELECT ON TABLE task TO readonly;


--
-- Name: tenant; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE tenant FROM PUBLIC;
REVOKE ALL ON TABLE tenant FROM bonita;
GRANT ALL ON TABLE tenant TO bonita;
GRANT ALL ON TABLE tenant TO agmednet;
GRANT SELECT ON TABLE tenant TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tenant TO "servicemix-securitymanager";
GRANT SELECT ON TABLE tenant TO readonly;


--
-- Name: theme; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE theme FROM PUBLIC;
REVOKE ALL ON TABLE theme FROM bonita;
GRANT ALL ON TABLE theme TO bonita;
GRANT ALL ON TABLE theme TO agmednet;
GRANT SELECT ON TABLE theme TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE theme TO "servicemix-securitymanager";
GRANT SELECT ON TABLE theme TO readonly;


--
-- Name: token; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE token FROM PUBLIC;
REVOKE ALL ON TABLE token FROM bonita;
GRANT ALL ON TABLE token TO bonita;
GRANT ALL ON TABLE token TO agmednet;
GRANT SELECT ON TABLE token TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE token TO "servicemix-securitymanager";
GRANT SELECT ON TABLE token TO readonly;


--
-- Name: trialmetadata; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE trialmetadata FROM PUBLIC;
REVOKE ALL ON TABLE trialmetadata FROM bonita;
GRANT ALL ON TABLE trialmetadata TO bonita;
GRANT SELECT ON TABLE trialmetadata TO readonlyuser;
GRANT SELECT ON TABLE trialmetadata TO readonly;


--
-- Name: trialxml; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE trialxml FROM PUBLIC;
REVOKE ALL ON TABLE trialxml FROM bonita;
GRANT ALL ON TABLE trialxml TO bonita;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE trialxml TO "servicemix-securitymanager";
GRANT SELECT ON TABLE trialxml TO readonlyuser;
GRANT SELECT ON TABLE trialxml TO readonly;


--
-- Name: user_; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE user_ FROM PUBLIC;
REVOKE ALL ON TABLE user_ FROM bonita;
GRANT ALL ON TABLE user_ TO bonita;
GRANT ALL ON TABLE user_ TO agmednet;
GRANT SELECT ON TABLE user_ TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE user_ TO "servicemix-securitymanager";
GRANT SELECT ON TABLE user_ TO readonly;


--
-- Name: user_contactinfo; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE user_contactinfo FROM PUBLIC;
REVOKE ALL ON TABLE user_contactinfo FROM bonita;
GRANT ALL ON TABLE user_contactinfo TO bonita;
GRANT ALL ON TABLE user_contactinfo TO agmednet;
GRANT SELECT ON TABLE user_contactinfo TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE user_contactinfo TO "servicemix-securitymanager";
GRANT SELECT ON TABLE user_contactinfo TO readonly;


--
-- Name: user_membership; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE user_membership FROM PUBLIC;
REVOKE ALL ON TABLE user_membership FROM bonita;
GRANT ALL ON TABLE user_membership TO bonita;
GRANT ALL ON TABLE user_membership TO agmednet;
GRANT SELECT ON TABLE user_membership TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE user_membership TO "servicemix-securitymanager";
GRANT SELECT ON TABLE user_membership TO readonly;


--
-- Name: userroletrialmetadata; Type: ACL; Schema: bonita; Owner: agmednet
--

REVOKE ALL ON TABLE userroletrialmetadata FROM PUBLIC;
REVOKE ALL ON TABLE userroletrialmetadata FROM agmednet;
GRANT ALL ON TABLE userroletrialmetadata TO agmednet;
GRANT SELECT ON TABLE userroletrialmetadata TO readonlyuser;
GRANT SELECT ON TABLE userroletrialmetadata TO readonly;


--
-- Name: waiting_event; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE waiting_event FROM PUBLIC;
REVOKE ALL ON TABLE waiting_event FROM bonita;
GRANT ALL ON TABLE waiting_event TO bonita;
GRANT ALL ON TABLE waiting_event TO agmednet;
GRANT SELECT ON TABLE waiting_event TO readonlyuser;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE waiting_event TO "servicemix-securitymanager";
GRANT SELECT ON TABLE waiting_event TO readonly;


--
-- Name: xmltype; Type: ACL; Schema: bonita; Owner: bonita
--

REVOKE ALL ON TABLE xmltype FROM PUBLIC;
REVOKE ALL ON TABLE xmltype FROM bonita;
GRANT ALL ON TABLE xmltype TO bonita;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE xmltype TO "servicemix-securitymanager";
GRANT SELECT ON TABLE xmltype TO readonlyuser;
GRANT SELECT ON TABLE xmltype TO readonly;


SET search_path = public, pg_catalog;

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ackstudy TO "servicemix-securitymanager";
GRANT SELECT ON TABLE ackstudy TO readonly;

REVOKE ALL ON TABLE appmanager FROM agmednet;
GRANT ALL ON TABLE appmanager TO agmednet;
GRANT SELECT,INSERT,DELETE,UPDATE ON ALL TABLES IN SCHEMA public TO "servicemix-securitymanager";
GRANT SELECT ON TABLE challengequestion TO readonly;
GRANT SELECT ON TABLE customoutput TO readonly;
GRANT SELECT ON TABLE customreport TO readonly;
GRANT SELECT ON TABLE customsearch TO readonly;
GRANT SELECT ON TABLE longitudinaldata TO readonly;
GRANT SELECT ON TABLE longitudinalmodality TO readonly;
GRANT SELECT ON TABLE pdependency TO readonly;
GRANT SELECT ON TABLE pdependencymapping TO readonly;
GRANT SELECT ON TABLE platform TO readonly;
GRANT SELECT ON TABLE platformcommand TO readonly;
GRANT SELECT ON TABLE submissionsworklist TO readonly;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE userrole TO "servicemix-adjudication";
REVOKE ALL ON TABLE report FROM "servicemix-securitymanager";
REVOKE ALL ON TABLE reportformat FROM "servicemix-securitymanager";
REVOKE ALL ON TABLE reportformatjrxml FROM "servicemix-securitymanager";
REVOKE ALL ON TABLE reportformattrialuserrolexref FROM "servicemix-securitymanager";
REVOKE ALL ON TABLE reportformattrialxref FROM "servicemix-securitymanager";
REVOKE ALL ON TABLE usersetting FROM "servicemix-securitymanager";
REVOKE ALL ON TABLE usersettingtype FROM "servicemix-securitymanager";

--
-- Name: report; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE report FROM PUBLIC;
REVOKE ALL ON TABLE report FROM agmednet;
GRANT ALL ON TABLE report TO agmednet;
GRANT SELECT ON TABLE report TO readonly;


--
-- Name: reportformat; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE reportformat FROM PUBLIC;
REVOKE ALL ON TABLE reportformat FROM agmednet;
GRANT ALL ON TABLE reportformat TO agmednet;
GRANT SELECT ON TABLE reportformat TO readonly;


--
-- Name: reportformatjrxml; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE reportformatjrxml FROM PUBLIC;
REVOKE ALL ON TABLE reportformatjrxml FROM agmednet;
GRANT ALL ON TABLE reportformatjrxml TO agmednet;
GRANT SELECT ON TABLE reportformatjrxml TO readonly;


--
-- Name: reportformattrialuserrolexref; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE reportformattrialuserrolexref FROM PUBLIC;
REVOKE ALL ON TABLE reportformattrialuserrolexref FROM agmednet;
GRANT ALL ON TABLE reportformattrialuserrolexref TO agmednet;
GRANT SELECT ON TABLE reportformattrialuserrolexref TO readonly;

--
-- Name: reportformattrialxref; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE reportformattrialxref FROM PUBLIC;
REVOKE ALL ON TABLE reportformattrialxref FROM agmednet;
GRANT ALL ON TABLE reportformattrialxref TO agmednet;
GRANT SELECT ON TABLE reportformattrialxref TO readonly;
GRANT SELECT ON TABLE sqcdata TO readonly;
GRANT ALL ON TABLE studydata TO "servicemix-sitelist";
GRANT ALL ON TABLE studydata TO "servicemix-triallist";
GRANT SELECT ON TABLE studydata TO readonly;
GRANT SELECT ON TABLE subjectversionxref TO readonly;
GRANT SELECT ON TABLE tenant TO readonly;
REVOKE ALL ON TABLE usersetting FROM PUBLIC;
REVOKE ALL ON TABLE usersetting FROM agmednet;
GRANT ALL ON TABLE usersetting TO agmednet;
GRANT SELECT ON TABLE usersetting TO readonly;


--
-- Name: usersettingtype; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE usersettingtype FROM PUBLIC;
REVOKE ALL ON TABLE usersettingtype FROM agmednet;
GRANT ALL ON TABLE usersettingtype TO agmednet;
GRANT SELECT ON TABLE usersettingtype TO readonly;


--
-- Name: usersroleontrial; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE usersroleontrial FROM PUBLIC;
REVOKE ALL ON TABLE usersroleontrial FROM agmednet;
GRANT ALL ON TABLE usersroleontrial TO agmednet;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE usersroleontrial TO "servicemix-securitymanager";
GRANT SELECT ON TABLE usersroleontrial TO readonly;

--
-- Name: usertrialprotocoltraining; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE usertrialprotocoltraining FROM PUBLIC;
REVOKE ALL ON TABLE usertrialprotocoltraining FROM agmednet;
GRANT ALL ON TABLE usertrialprotocoltraining TO agmednet;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE usertrialprotocoltraining TO "servicemix-securitymanager";
GRANT SELECT ON TABLE usertrialprotocoltraining TO readonly;


--
-- Name: usertrialrolexref; Type: ACL; Schema: public; Owner: agmednet
--

REVOKE ALL ON TABLE usertrialrolexref FROM PUBLIC;
REVOKE ALL ON TABLE usertrialrolexref FROM agmednet;
GRANT ALL ON TABLE usertrialrolexref TO agmednet;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE usertrialrolexref TO "servicemix-securitymanager";
GRANT SELECT ON TABLE usertrialrolexref TO readonly;
