-- JUDI-5648
update trialsite set clinicaltrialsiteid='DISABLED-' || clinicaltrialsiteid WHERE (trial_id,clinicaltrialsiteid) IN (select trial_id, TRIM (LEADING '0'FROM clinicaltrialsiteid) as clinicaltrialsiteid from trialsite join trial on trialsite.trial_id = trial.id WHERE trialsite.trial_id IN (select distinct trial_id FROM study where timeuploaded >= '2015-05-02') GROUP by 1,2 HAVING count(*) > 1);

DO $body$
DECLARE r record;
BEGIN
    FOR r IN
SELECT
    distinct tc.constraint_name, tc.table_name, tc.table_schema
FROM
    information_schema.table_constraints AS tc
    JOIN information_schema.key_column_usage AS kcu
      ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage AS ccu
      ON ccu.constraint_name = tc.constraint_name
    WHERE constraint_type = 'FOREIGN KEY'
 LOOP
       EXECUTE 'ALTER TABLE ' || quote_ident(r.table_schema) || '.' || quote_ident(r.table_name)|| ' DROP CONSTRAINT '|| quote_ident(r.constraint_name) || ';';
    END LOOP;
END
$body$;

CREATE OR REPLACE FUNCTION truncate_schema(_schema character varying)
  RETURNS void AS
$BODY$
declare
    selectrow record;
begin
for selectrow in
select 'TRUNCATE TABLE ' || quote_ident(_schema) || '.' ||quote_ident(t.table_name) || ';' as qry 
from (
     SELECT table_name 
     FROM information_schema.tables
     WHERE table_type = 'BASE TABLE' AND table_schema = _schema
     )t
loop
execute selectrow.qry;
end loop;
end;
$BODY$
  LANGUAGE plpgsql;

SELECT truncate_schema('bonita');

drop function truncate_schema(_schema varchar);
-- delete judi stuff from imaging
update public.trial set productbitmask =3 where id=434591374;
delete from siteversionxref where site_id in (select id from trialsite where trial_id in (select id from trial where productbitmask=2));
delete from subjectsitexref where site_id in (select id from trialsite where trial_id in (select id from trial where productbitmask=2));
delete from usersroleontrial where trial_id in (select id from trial where productbitmask = 2);
delete from trialsite where trial_id in (select id from trial where productbitmask = 2);
delete from trialversion where trial_id in (select id from trial where productbitmask = 2);
delete from trialsubject where trial_id in (select id from trial where productbitmask = 2);
delete from notificationlist where trial_id in (select id from trial where productbitmask = 2);
delete from portalemaillog where trial_id in (select id from trial where productbitmask = 2);
delete from usertrialrolexref where trial_id in (select id from trial where productbitmask = 2);
delete from pendingtrialuser where trial_id in (select id from trial where productbitmask = 2);
delete from clinicaltrialpermission where targettrial_id in (select id from trial where productbitmask = 2);
delete from trial where productbitmask = 2;

--update tlapeer
update useraccount set emailaddress='research@cvrcnm.com' where username='tlapeer';
--update rmendoza
update useraccount set username='rmendoza1' where username='rmendoza';
--update jlw
update useraccount set username='jlw1' where username='jlw';
-- update delvalm
update useraccount set username='delvalm@ccf.org' where username='delvalm';
--update "gkov"(different username)
update useraccount set username='gkovacs' where username='gabor.kovacs';
--update "mkato"
update useraccount set username='mkato1' where username='mkato';
--update gthomas
update useraccount set username ='gthomas1' where username='gthomas';
-- update rhendrickson
update useraccount set username ='reneeh' where username='rhendrickson';
-- update mwilliamson
update useraccount set username ='mwilliamson1' where username='mwilliamson';
-- update sparsons
update useraccount set username ='sparsons1' where username='sparsons';
-- update cdubois
update useraccount set username='crystal.dubois' where username='cdubois';
-- update doliver
update useraccount set emailaddress ='doliver@iacthealth.com' where username='doliver';
-- update pmol66
update useraccount set username ='pmol' where username ='pmol66';
-- update i.vanstein@olvg.nl
update useraccount set username='irenevs' where username ='i.vanstein@olvg.nl';
-- update yradstake
update useraccount set username='yorrad' where username ='yradstake';
-- update arkadiusz supczinski
update useraccount set username='a_supczinski' where username='arkadiusz supczinski';
-- update d.seigers@mzh.nl
update useraccount set username ='d.seigers' where username ='d.seigers@mzh.nl';
-- update nc
update useraccount set username='nsk61' where username='nkeane';
-- update minjikim
update useraccount set username='minjikim1' where username='minjikim';
-- update serickson
update useraccount set username='serickson1' where username='serickson';
-- update ledwards
update useraccount set username='ledwards1' where username='ledwards';
-- update jvelazquez
update useraccount set username ='jvelazquez1' where username='jvelazquez';
-- update hbui
update useraccount set username='hbui1' where username='hbui';
-- update pharma@mavkorhaz-szolnok.hu
update useraccount set username='efarkasne' where username='pharma@mavkorhaz-szolnok.hu';
-- update tthompson
update useraccount set username='tthompson1' where username='tthompson';
-- update relfagui
update useraccount set emailaddress ='rime.elfagui@astrazeneca.com' where username='relfagui';
-- update mhussain
update useraccount set username='mhussain1' where username='mhussain';
-- update cheard
update useraccount set username ='ciarradh' where username='cheard';
-- update mjackson
update useraccount set username='mjackson1' where username='mjackson';
-- update mbergeron
update useraccount set username='mbergeron1' where username='mbergeron';
-- update r.turan@olvg.nl
update useraccount set username='rturan' where username='r.turan@olvg.nl';
-- update jstewart
update useraccount set username='jstewart1' where username='jstewart';

-- update overlapped ctp
update clinicaltrialpermission set id=602457442 where id=602457441;
-- delete users who don't have ctp 
delete from useraccountuserrolexref where useraccount_id not in (select distinct therecipient_id from clinicaltrialpermission);

-- update judiadmin
update useraccount set username='judiadmin' where username='judi';

--update jjonk
update useraccount set username='judi' where username ='jjonk';
-- update datkinson
update useraccount set username ='dawnatkinson' where username='datkinson';
-- update yasmingc
update useraccount set emailaddress ='ipr_yasmin@yahoo.com.ar' where username='yasmingc';
-- update sgold
update useraccount set username ='sgold1' where username='sgold';
-- update ddawson
update useraccount set username='ddawson1' where username='ddawson';
-- update ysekiguchi
update useraccount set username='ysekiguchi' where username='ytsuyai';
-- update these emailaddress on last run

update useraccount set username='zhrubi1' where username='zhrubi';
update useraccount set username='ykim1' where username='ykim';
update useraccount set username='pgarcia1' where username='pgarcia';
update useraccount set username='ddoolittle1' where username='ddoolittle';
update useraccount set username='lselker1' where username='lselker';
update useraccount set username='bpatel1' where username='bpatel';
update useraccount set username='apark1' where username='apark';
update useraccount set username='tamara1' where username='tamara';
update useraccount set username='katalin1' where username='katalin';

--same user
update useraccount set username='fkelsall1' where username='fkelsall';
-- same user
update useraccount set username='cblanton1586@gmail.com' where username='cblanton';
-- samw user
update useraccount set username='he12280' where username='mary.vorster';
-- same user
update useraccount set username='srodriguez89' where username='rodriguezs';
-- same user
update useraccount set username='timea' where username='timea.sumegi';
-- same user
update useraccount set username='matiasmun' where username='mnmungo';
-- same user
update useraccount set username='cruiz18' where username='cruiz91';
-- same user
update useraccount set username='rosaliedixon@radiantresearch.com' where username='rdixon';
-- same user
update useraccount set username='jenny.hill' where username='jhill1969';
-- delete all from userrole
delete from userrole;

--update "coordinator"
update useraccount set username ='coordinator1' where username='coordinator';

-- update "mjlee"
update useraccount set username ='mjlee1' where username='mjlee';

-- wierd issue
delete from customoutput where id=465865376;

-- JUDI-5752(fixed using new index)
--update historyevent set xml='<desktopAgent>' || xml || '</desktopAgent>' WHERE service='desktopAgent';
--update historyevent set xml=replace(xml,'Demo''''','Demo''') WHERE service='desktopAgent';
--update historyevent set xml=replace(xml,'</description><id><sponsor>','</description><sponsor>') where module in ('com.agmednet.triallist.impl.TrialListImpl','com.agmednet.triallist.model.Trial');
