# This is the repository of db migration script
# Migration Cheatsheet
Stop imaging Portal/Core

# Take a snapshot of imaging DB replication volume

# Restore and attach the new volume to 34.230.92.46, also convert replication to standalone
# *** DO next steps(db restore) in parallel ***
1. /var/lib/pgsql/9.0/data/recovery.conf -> standby_mode = 'off'
2. /var/lib/pgsql/9.0/data/postgresql.conf -> archive_mode = off, shared_buffer -> 30 , effective_cache -> 80
3. restart postgres

# Backup Judi DB and restore it in VPC: 

# Dump imaging OpenDJ: (Do this anytime)
(Apache Directory Studio) ldif export

# Make changes to ldif file:
1. remove header and UID=0
2. update: 
	*rmendoza* 
	*jlw* 
	*mkato* 
	*gthomas* 
	*mwilliamson* 
	*sparsons* 
	*minjikim*
	*serickson* 
	*ledwards*
	*jvelazquez* 
	*hbui* 
	*tthompson* 
	*mhussain*
	*mjackson*
	*mbergeron* 
	*jstewart*
	*sgold* 
	*ddawson*
	*zhrubi*
	*ykim*
	*pgarcia*
	*ddoolittle*
	*lselker*
	*bpatel*
	*apark*
	*tamara*
	*katalin*
	*mjlee*
	*coordinator*
3. remove:
	*delvalm*
	*gabor.kovacs*
	*rhendrickson*
	*cdubois*
	*pmol66*
	*i.vanstein@olvg.nl*
	*yradstake*
	*arkadiusz supczinski*
	*d.seigers@mzh.nl*
	*nkeane*
	*pharma@mavkorhaz-szolnok.hu*
	*cheard*
	*r.turan@olvg.nl*
	*jjonk*
	*datkinson*
	*ytsuyai*
	*cblanton*
	*mary.vorster*
	*rodriguezs*
	*timea.sumegi*
	*mnmungo*
	*cruiz91*
	*rdixon*
	*jhill1969*
	*fkelsall*

# Import ldif: (agmednet, standalone)
(local) ldapmodify -x -H ldap://judi-ldap.agmednet.net:1389/ -D "cn=Directory Manager" -W -f prod.ldif -v -a -c

# Make schema changes to imaging db
# *** Do next step in parallel ***
1. psql -U hwang portal_db < dbmerge_with_dbscripts.sql 2>&1 | tee step22_schema1.log
2. psql -U hwang portal_db < postjudi12.sql 2>&1 | tee step22_schema2.log

# Pre run on Judi db: 
psql -h <judi_db> -U master portal_db < pre-run.sql 2>&1 | tee step23_prerunresult.log

# Pre run on imaging db:
psql -h <target_img_db> -U hwang portal_db < delete-script.sql 2>&1 | tee step24_prerunimg.log

# Run graph
# *** Make sure to change the addr in graph.py. INIT could be false if re-run the script ***
# *** ALSO, make sure delete all dumplist+historyevent.sql file ***
./graph.py 2>&1 | tee step25_compare.log

# dump&reload lo (We can do this anytime, and we need servicemix added to pgpass)
1. ./lo.sh dump 127.0.0.1
2. remove 1st lo
3. ./lo.sh load <judi_db>

# dump&reload these in order, add master to pgpass
1. move studyform down after study
2. add ctp(we could handle this in graph too, which also provides us safer reload)
2. ./dump.sh 127.0.0.1 <judi_db>

# restore constaints
psql -h <judi_db> -U master portal_db < post-run.sql 2>&1 | tee step<something>_postrunresult.log

# set new seq value
alter sequence hibernate_sequence restart with <something>; 

# update historyevent after everything
psql -h <judi_db> -U master portal_db < historyevent.sql 2>&1 | tee historyevent.log
