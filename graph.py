#!/usr/bin/python
import psycopg2
import networkx as nx
#import matplotlib.pyplot as plt
import Queue
import re
import sys
from xml.etree import ElementTree as et

class SetQueue(Queue.Queue):

    def _init(self, maxsize):
        self.queue = set()

    def _put(self, item):
        self.queue.add(item)

    def _get(self):
	val = self.queue.pop()
        return val


STARTING_NODE = 'public.webservice'
JUDI_DBADDR = "judi-prod-1.cq5cdv8wsvzi.us-east-1.rds.amazonaws.com"
IMG_DBADDR = "127.0.0.1"
JUDI_USER = "master"
JUDI_PWD = "Agmednet1!"
IMG_USER = "hwang"
IMG_PWD = "whch900422"
INITIAL = False

def main():
    node_queue = SetQueue()
    judi_conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
    cur = judi_conn.cursor()
    current_vertex = STARTING_NODE
    # Queue needs to have a starting entry to enter the loop below.
    node_queue.put(current_vertex)
    node_queue.put('public.trial')
    node_queue.put('public.userrole')
    node_queue.put('public.trialsite')
    node_queue.put('public.trialsubject')
    node_queue.put('public.corelab')
    node_queue.put('public.sponsor')
    node_queue.put('public.country')
    node_queue.put('public.portalemailtemplate')
    node_queue.put('public.federationpartner')
    node_queue.put('public.attachment')
    G = nx.DiGraph()
    while not node_queue.empty():
        try:
	    cur.execute("""
			SELECT tc.table_schema || '.' || tc.table_name, kcu.column_name
			FROM information_schema.table_constraints tc
			JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
			JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
			WHERE constraint_type = 'FOREIGN KEY'
			AND(tc.table_schema='public' OR (SELECT schemaname FROM pg_tables WHERE tablename=ccu.table_name LIMIT 1)='public')
			AND ccu.table_name='%s' AND ccu.table_schema='%s'
			""" % (current_vertex.split('.',2)[1], current_vertex.split('.',2)[0]))
            #tuple[0]: table name, tuple[1]: fk column name
	    for tuple in cur.fetchall():
		edge = tuple[0]
                G.add_edge(current_vertex, edge)
		edgeInfo = set()
		if G.edge[current_vertex][edge]:
		    edgeInfo = G.edge[current_vertex][edge]['fk']
		edgeInfo.add(tuple[1])
		G.edge[current_vertex][edge] = {'fk': edgeInfo}
		if edge != "public.study":
                    node_queue.put(edge)
        except psycopg2.Error as e:
            print("Error creating graph: %s" % e)
        # Remove an entry from the queue.
        current_vertex = node_queue.get()
    #f = plt.figure(figsize=(24,24))
    #nx.draw_networkx(G, pos=nx.random_layout(G), ax=f.add_subplot(111), node_size=600)
    #f.savefig("table_graph.png", format="PNG")
    cur.close()
    judi_conn.close()
    #print nx.topological_sort(G)

    for tablename in nx.topological_sort(G):
        try:
            f = open("dumplist","a")
            f.write(tablename+"\n")
            f.close()
        except:
            print("IO error")
	if tablename == "public.historyevent" or tablename == "public.portalemaillog" or tablename =="public.oldpassword":
	    if INITIAL:
                updatePK(tablename)
	    continue
	if "xref" not in tablename and "bonita." not in tablename:
	    if tablename != "public.subjecttrialversionxeref" and tablename != "public.submissionsworklist" and tablename !="public.portalemaillog" and tablename != "public.study":
		findingindices(tablename,G)
	continue
	#nx.write_graphml(G, "/home/ec2-user/table_graph.graphml")

def updatePK(tablename):
    maxJudi = findMaxId(tablename,"judi")
    maxImg = findMaxId(tablename,"img")
    maxPk = max(maxJudi, maxImg)
    try:
	conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
	conn.autocommit = True
	cur = conn.cursor()
	cur.execute("SELECT id FROM %s order by 1 limit 1" % tablename)
	minJudi = cur.fetchone()[0]
        if minJudi < maxPk:
	    offSet = maxPk - minJudi + 1
	    cur.execute("UPDATE %s SET id = id + %s;" % (tablename, str(offSet)))
	cur.close()
        conn.close()
    except psycopg2.Error as e:
        print("Unable to update pks. table: "+tablename)
        print(e.pgerror)
        sys.exit()

def updateRef(G,db,tablename,max_id,tmpId):
    try:
        if db == "judi":
	    judi_conn2 = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
	else:
            judi_conn2 = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
        judi_conn2.autocommit = True
	cur = judi_conn2.cursor()
	for edge in G.edge[tablename]:
        # run update fks only when there is a reference
            if G.edge[tablename][edge]['fk']:
                update_query = "UPDATE "+edge+" SET "
                for column_name in G.edge[tablename][edge]['fk']:
                    final_query= update_query+column_name+"="+str(max_id)+" WHERE "+column_name+"="+tmpId
                    # Execute this query in both DBs.
                    cur.execute(final_query)
        cur.close()
        judi_conn2.close()
    except psycopg2.Error as e:
        print("Unable to run: %s" % final_query)
        print(e.pgerror)
	sys.exit()

def updateTablePk(tablename,max_id,tmpId):
    try:
        judi_conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
        judi_conn.autocommit = True
        cur = judi_conn.cursor()
        cur.execute("UPDATE %s SET id = %s WHERE id = %s" % (tablename,max_id,tmpId))
        cur.close()
        judi_conn.close()
    except psycopg2.Error as e:
        print("Unable update pk for: %s PK is %s" % (tablename,max_id))
        print(e.pgerror)
        sys.exit()

def insertOrDeleteRecord(tablename,max_id,iod):
    try:
	judi_conn3 = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
	judi_conn3.autocommit = True
        cur = judi_conn3.cursor()
	if iod == "insert":
	    cur.execute("INSERT INTO %s(id) VALUES(%s)" % (tablename, max_id))
	elif iod == "delete":
	    cur.execute("DELETE FROM %s WHERE id = %s" % (tablename, max_id))
	else:
	    print "Illegal option."
        cur.close()
        judi_conn3.close()
    except psycopg2.Error as e:
        print "Unable to run insertion/deletion"
        print(e.pgerror)
	sys.exit()
	
def findMaxId(tablename,dbname):
    try:
	if dbname == "judi":
	    conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
	    cur = conn.cursor()
	elif dbname == "img":
	    conn = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
	    cur = conn.cursor()
	else:
	    print "No DB selected."
	    sys.exit()
	cur.execute("SELECT max(id) FROM %s" % tablename)
	max_id = cur.fetchone()[0]
	cur.close()
	conn.close()
	return 0 if max_id is None else max_id
    except psycopg2.Error as e:
        print "Unable to find max id. FROM "+dbname+" table: "+tablename
        print(e.pgerror)
        sys.exit()
	
def updateHistory(dest,src,tablename,dbname):
    try:
        if dbname == "judi":
	    judi_conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
	    judi_conn.autocommit = True
            cur = judi_conn.cursor()
            cur.execute("UPDATE public.historyevent SET xml=replace(xml,'>%s<','>%s<') WHERE xml like '%%>%s<%%'" % (src,dest,src))
        else:
            q = "UPDATE public.historyevent SET xml=replace(xml,'>" + src + "<','>" + dest + "<') WHERE xml like '%>" + src + "<%';" + "\n" 
            f2 = open("historyevent.sql","a")
            f2.write(q)
            f2.close()
    except:
	print "Unable to update historyevent record."
	sys.exit()

def deleteImgRecord(tablename,tid):
    try:
	img_conn2 = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
	img_conn2.autocommit = True
	cur2 = img_conn2.cursor()
	cur2.execute("DELETE FROM %s WHERE id = %s" % (tablename,tid))
    except psycopg2.Error as e:
	print "Unable to delete record in imaging db."
	print(e.pgerror)
	sys.exit()

def updateCtp(tablename,db,tid,oid):
    try:
        if db =="judi":
	    judi_conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
	else:
            judi_conn = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
        judi_conn.autocommit = True
	cur = judi_conn.cursor()
	if tablename == "public.trial":
	    cur.execute("UPDATE clinicaltrialpermission SET targettrial_id= %s WHERE targettrial_id= %s" % (tid,oid))
	elif tablename == "public.trialsite":
	    cur.execute("UPDATE clinicaltrialpermission SET targettrialsite_id= %s WHERE targettrialsite_id= %s" % (tid,oid))
	elif tablename == "public.useraccount":
	    cur.execute("UPDATE clinicaltrialpermission SET therecipient_id= %s WHERE therecipient_id= %s" % (tid,oid))
	else:
	    print "Not a valid tablename in CTP."
    except psycopg2.Error as e:
	print "Unable to update CTP."
	print(e.pgerror)
	sys.exit()

def updateImgRecord(tablename,tid,oid):
    try:
        img_conn2 = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
        img_conn2.autocommit = True
        cur2 = img_conn2.cursor()
        cur2.execute("UPDATE %s SET id = %s WHERE id = %s" % (tablename,tid,oid))
    except psycopg2.Error as e:
        print "Unable to update record in imaging db."
        print(e.pgerror)
        sys.exit()

def compareResult(query,tablename,G):
    try:
    	judi_conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
        cur = judi_conn.cursor()
	cur.execute(query)
	img_conn = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
        cur2 = img_conn.cursor()
        cur2.execute(query)
	judi_results = cur.fetchall()
	if not judi_results:
	    return
	img_results = cur2.fetchall()
        user_dic = {}
        if tablename == "public.useraccount" or tablename == "public.portalemailtemplate":
	    for result in img_results:
                user_dic[result[1]] = result[0]
            for i in range(len(judi_results)-1):
            # deal with useraccount first, some users may have different pks but same username and emailaddr
                if user_dic.has_key(judi_results[i][1]):
                    if judi_results[i][0] != user_dic[judi_results[i][1]]:
                        updateRef(G,"img",tablename,judi_results[i][0],str(user_dic[judi_results[i][1]]))
                        updateHistory(str(judi_results[i][0]),str(user_dic[judi_results[i][1]]),tablename,"img")
                        if tablename == "public.useraccount":
                            updateCtp(tablename,"img",str(judi_results[i][0]),str(user_dic[judi_results[i][1]]))
                        updateImgRecord(tablename,str(judi_results[i][0]),str(user_dic[judi_results[i][1]]))
                        print("updated " + judi_results[i][1] + " from " + str(user_dic[judi_results[i][1]]) + " to " + str(judi_results[i][0]))
	img_conn2 = connect(IMG_DBADDR,IMG_USER,IMG_PWD)
        cur3 = img_conn2.cursor()
        cur3.execute(query)
        img_results = cur3.fetchall()
        img_set = set(img_results)
        for i in range(len(judi_results)-1):
            if judi_results[i] not in img_set:
	    # only update record when there is an overlap
	        cur2.execute("SELECT id FROM %s WHERE id=%s" %(tablename,judi_results[i][0]))
	        img_results = cur2.fetchall()
	        if img_results:
		    max_id_jd = findMaxId(tablename,"judi")
		    max_id_img = findMaxId(tablename,"img")
		    # get the max value of 2 DB to make them compatiable
		    max_id = max(max_id_jd,max_id_img)
		    # placeholder id
		    max_id = max_id + 2
		    insertOrDeleteRecord(tablename, str(max_id), "insert")
		    updateRef(G,"judi",tablename,max_id,str(judi_results[i][0]))
		    # Update pk after reference changes, move id
		    updateTablePk(tablename,str(max_id-1),str(judi_results[i][0]))
		    # point all referenced table back
		    updateRef(G,"judi",tablename,max_id-1,str(max_id))
		    updateHistory(str(max_id-1),str(judi_results[i][0]),tablename,"judi")
		    insertOrDeleteRecord(tablename, str(max_id), "delete")
		    if tablename == "public.trial" or tablename == "public.trialsite" or tablename == "public.useraccount":
		        updateCtp(tablename,"judi",str(max_id-1),str(judi_results[i][0]))
		    print "updated "+ tablename + " id: "+ str(judi_results[i][0])
	    else:
		deleteImgRecord(tablename,str(judi_results[i][0]))
                if tablename == "public.useraccount":
                    judi_conn2 = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
                    judi_conn2.autocommit = True
                    cur4 = judi_conn2.cursor()
                    cur4.execute("DELETE FROM public.useraccountuserrolexref WHERE userrole_id IN (select id from userrole where productbitmask IN (0,1)) AND useraccount_id=%s" % str(judi_results[i][0]))
                    cur4.close()
                    judi_conn2.close()
                    print("removed user"+str(judi_results[i][0])+"useraccountuserrole permission")
	print("finished "+ tablename)
        cur3.close()
        img_conn2.close()
	cur2.close()
	img_conn.close()
	cur.close()
        judi_conn.close()
    except psycopg2.Error as e:
        print("Error running: %s" % query)
	print(e.pgerror)
        sys.exit()

# Find all unique indexes in a table.
def findingindices(tablename,G):
    try:
	judi_conn = connect(JUDI_DBADDR,JUDI_USER,JUDI_PWD)
    	cur = judi_conn.cursor()
	# This will fetch all unique indexes on a given table.
	cur.execute("SELECT indexdef from pg_indexes where tablename = '%s' AND schemaname = '%s' AND indexdef like 'CREATE UNIQUE INDEX %s' ORDER BY length(split_part(indexdef,'(',2))" % (tablename.split('.',2)[1],tablename.split('.',2)[0],'%'))
	column_names = cur.fetchall()
	# If there is nothing to compare, skip it.
	if not column_names:
	    return
	# Create a generic query
	q = "SELECT "
	for col in column_names:
	    # Only fetch column names enclosed in parentheses.
	    q = q+ str(col[0]).split('(',2)[1].split(')',2)[0]+ ","
	query = q[:-1]+" FROM "+tablename+" ORDER BY 1;"
	# Compare Judi and Imaging results.
	compareResult(query,tablename,G)
	cur.close()
        judi_conn.close()
    except psycopg2.Error as e:
	print("Error getting indices on table %s" % tablename)
	sys.exit()

def connect(dbaddr,user,pwd):
    conn = psycopg2.connect(
        host=dbaddr,
        database="portal_db",
        user=user,
        password=pwd)
    return conn

if __name__ == "__main__":
    main()
